package com.dlazaridis.pollfishwebview.app;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.dlazaridis.pollfishwebview.PollfishWebView;
import com.dlazaridis.pollfishwebview.app.R;
import com.dlazaridis.pollfishwebview.enums.Position;

public class MainActivity extends AppCompatActivity {

  @Override protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.main);
  }

  public void onClick(View view) {
    if (view.getId() == R.id.webviewButton) {
      new PollfishWebView.Builder(this)
              .params("Param 1", "Param 2","Param 3","Param 4","Param 5")
              .webViewOffscreenPreRaster(true)
              .showProgressBar(true)
              .progressBarPosition(Position.TOP_OF_TOOLBAR)
              .load();
    }
  }
}
