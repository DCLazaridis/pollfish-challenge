package com.dlazaridis.pollfishwebview;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.AnimRes;
import android.support.annotation.ArrayRes;
import android.support.annotation.ColorInt;
import android.support.annotation.ColorRes;
import android.support.annotation.DimenRes;
import android.support.annotation.DrawableRes;
import android.support.annotation.NonNull;
import android.support.annotation.StringRes;
import android.support.design.widget.AppBarLayout.LayoutParams.ScrollFlags;
import android.webkit.WebSettings;

import com.dlazaridis.pollfishwebview.enums.Position;
import com.dlazaridis.pollfishwebview.listeners.BroadCastManager;
import com.dlazaridis.pollfishwebview.listeners.WebViewListener;
import com.dlazaridis.pollfishwebview.utils.content.Ctx;
import com.dlazaridis.pollfishwebview.utils.content.Res;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Dimitrios Lazaridis on 13/02/2018.
 */
public class PollfishWebView {

  public static class Builder implements Serializable {

    protected final transient Context context;
    protected transient List<WebViewListener> listeners = new ArrayList<>();

    protected Integer key;

    protected Integer statusBarColor;

    protected Integer toolbarColor;
    protected Integer toolbarScrollFlags;

    protected Integer iconDefaultColor;
    protected Integer iconDisabledColor;
    protected Integer iconPressedColor;
    protected Integer iconSelector;

    protected Boolean showIconClose;
    protected Boolean disableIconClose;

    protected Boolean showSwipeRefreshLayout;
    protected Integer swipeRefreshColor;
    protected Integer[] swipeRefreshColors;

    protected Boolean showProgressBar;
    protected Integer progressBarColor;
    protected Float progressBarHeight;
    protected Position progressBarPosition;

    protected String param1;
    protected Float param1Size;
    protected String param1Font;
    protected Integer param1Color;

    protected String param2;
    protected Float param2Size;
    protected String param2Font;
    protected Integer param2Color;

    protected String param3;
    protected String param4;
    protected String param5;

    protected Integer animationOpenEnter = R.anim.slide_left_in;
    protected Integer animationOpenExit = R.anim.slide_left_out;
    protected Integer animationCloseEnter = R.anim.slide_right_in;
    protected Integer animationCloseExit = R.anim.slide_right_out;

    protected Boolean backPressToClose;

    protected Boolean webViewSupportZoom;
    protected Boolean webViewMediaPlaybackRequiresUserGesture;
    protected Boolean webViewBuiltInZoomControls;
    protected Boolean webViewDisplayZoomControls;
    protected Boolean webViewAllowFileAccess;
    protected Boolean webViewAllowContentAccess;
    protected Boolean webViewLoadWithOverviewMode;
    protected Boolean webViewSaveFormData;
    protected Integer webViewTextZoom;
    protected Boolean webViewUseWideViewPort;
    protected Boolean webViewSupportMultipleWindows;
    protected WebSettings.LayoutAlgorithm webViewLayoutAlgorithm;
    protected String webViewStandardFontFamily;
    protected String webViewFixedFontFamily;
    protected String webViewSansSerifFontFamily;
    protected String webViewSerifFontFamily;
    protected String webViewCursiveFontFamily;
    protected String webViewFantasyFontFamily;
    protected Integer webViewMinimumFontSize;
    protected Integer webViewMinimumLogicalFontSize;
    protected Integer webViewDefaultFontSize;
    protected Integer webViewDefaultFixedFontSize;
    protected Boolean webViewLoadsImagesAutomatically;
    protected Boolean webViewBlockNetworkImage;
    protected Boolean webViewBlockNetworkLoads;
    protected Boolean webViewJavaScriptEnabled;
    protected Boolean webViewAllowUniversalAccessFromFileURLs;
    protected Boolean webViewAllowFileAccessFromFileURLs;
    protected String webViewGeolocationDatabasePath;
    protected Boolean webViewAppCacheEnabled;
    protected String webViewAppCachePath;
    protected Boolean webViewDatabaseEnabled;
    protected Boolean webViewDomStorageEnabled;
    protected Boolean webViewGeolocationEnabled;
    protected Boolean webViewJavaScriptCanOpenWindowsAutomatically;
    protected String webViewDefaultTextEncodingName;
    protected String webViewUserAgentString;
    protected Boolean webViewNeedInitialFocus;
    protected Integer webViewCacheMode;
    protected Integer webViewMixedContentMode;
    protected Boolean webViewOffscreenPreRaster;

    protected String injectJavaScript;

    protected String mimeType;
    protected String encoding;
    protected String data;
    protected String url = "http://google.com";
    protected WebViewListener defaultWebViewListenerWebViewListener;

    public Builder(@NonNull Activity activity) {
      this.context = activity;
      //addDefaultLisener();
      Base.initialize(activity);
    }

    /**
     * If you use context instead of activity, the WebView won't be able to override activity
     * animation.
     * Try to create builder with Activity if it's possible.
     */
    public Builder(@NonNull Context context) {
      this.context = context;
      //addDefaultLisener();
      Base.initialize(context);
    }

//    private void addDefaultLisener() {
//      this.listeners.add(new WebViewListener() {
//        @Override
//        public void onProgressChanged(int progress) {
//          super.onProgressChanged(progress);
//        }
//
//        @Override
//        public void onReceivedTitle(String title) {
//          super.onReceivedTitle(title);
//        }
//
//        @Override
//        public void onReceivedTouchIconUrl(String url, boolean precomposed) {
//          super.onReceivedTouchIconUrl(url, precomposed);
//        }
//
//        @Override
//        public void onPageStarted(String url) {
//          super.onPageStarted(url);
//        }
//
//        @Override
//        public void onPageFinished(String url) {
//          super.onPageFinished(url);
//        }
//
//        @Override
//        public void onLoadResource(String url) {
//          super.onLoadResource(url);
//        }
//
//        @Override
//        public void onPageCommitVisible(String url) {
//          super.onPageCommitVisible(url);
//        }
//
//        @Override
//        public void onDownloadStart(String url, String userAgent, String contentDisposition, String mimeType, long contentLength) {
//          super.onDownloadStart(url, userAgent, contentDisposition, mimeType, contentLength);
//        }
//      });
//    }

    public Builder setWebViewListener(WebViewListener listener) {
      listeners.clear();
      listeners.add(listener);
      return this;
    }

    public Builder addWebViewListener(WebViewListener listener) {
      listeners.add(listener);
      return this;
    }

    public Builder removeWebViewListener(WebViewListener listener) {
      listeners.remove(listener);
      return this;
    }

    public Builder statusBarColor(@ColorInt int color) {
      this.statusBarColor = color;
      return this;
    }

    public Builder statusBarColorRes(@ColorRes int colorRes) {
      this.statusBarColor = Res.getColor(colorRes);
      return this;
    }

    public Builder toolbarColor(@ColorInt int color) {
      this.toolbarColor = color;
      return this;
    }

    public Builder toolbarColorRes(@ColorRes int colorRes) {
      this.toolbarColor = Res.getColor(colorRes);
      return this;
    }

    public Builder toolbarScrollFlags(@ScrollFlags int flags) {
      this.toolbarScrollFlags = flags;
      return this;
    }

    public Builder iconDefaultColor(@ColorInt int color) {
      this.iconDefaultColor = color;
      return this;
    }

    public Builder iconDefaultColorRes(@ColorRes int color) {
      this.iconDefaultColor = Res.getColor(color);
      return this;
    }

    public Builder iconDisabledColor(@ColorInt int color) {
      this.iconDisabledColor = color;
      return this;
    }

    public Builder iconDisabledColorRes(@ColorRes int colorRes) {
      this.iconDisabledColor = Res.getColor(colorRes);
      return this;
    }

    public Builder iconPressedColor(@ColorInt int color) {
      this.iconPressedColor = color;
      return this;
    }

    public Builder iconPressedColorRes(@ColorRes int colorRes) {
      this.iconPressedColor = Res.getColor(colorRes);
      return this;
    }

    public Builder iconSelector(@DrawableRes int selectorRes) {
      this.iconSelector = selectorRes;
      return this;
    }

    public Builder showIconClose(boolean showIconClose) {
      this.showIconClose = showIconClose;
      return this;
    }

    public Builder disableIconClose(boolean disableIconClose) {
      this.disableIconClose = disableIconClose;
      return this;
    }

    public Builder showSwipeRefreshLayout(boolean showSwipeRefreshLayout) {
      this.showSwipeRefreshLayout = showSwipeRefreshLayout;
      return this;
    }

    public Builder swipeRefreshColor(@ColorInt int color) {
      this.swipeRefreshColor = color;
      return this;
    }

    public Builder swipeRefreshColorRes(@ColorRes int colorRes) {
      this.swipeRefreshColor = Res.getColor(colorRes);
      return this;
    }

    public Builder swipeRefreshColors(int[] colors) {
      Integer[] swipeRefreshColors = new Integer[colors.length];
      for (int i = 0; i < colors.length; i++)
        swipeRefreshColors[i] = colors[i];
      this.swipeRefreshColors = swipeRefreshColors;
      return this;
    }

    public Builder swipeRefreshColorsRes(@ArrayRes int colorsRes) {
      int[] colors = Res.getIntArray(colorsRes);
      return swipeRefreshColors(colors);
    }

    public Builder showProgressBar(boolean showProgressBar) {
      this.showProgressBar = showProgressBar;
      return this;
    }

    public Builder progressBarColor(@ColorInt int color) {
      this.progressBarColor = color;
      return this;
    }

    public Builder progressBarColorRes(@ColorRes int colorRes) {
      this.progressBarColor = Res.getColor(colorRes);
      return this;
    }

    public Builder progressBarHeight(float height) {
      this.progressBarHeight = height;
      return this;
    }

    public Builder progressBarHeight(int height) {
      this.progressBarHeight = (float) height;
      return this;
    }

    public Builder progressBarHeightRes(@DimenRes int height) {
      this.progressBarHeight = Res.getDimension(height);
      return this;
    }

    public Builder progressBarPosition(@NonNull Position position) {
      this.progressBarPosition = position;
      return this;
    }

    public Builder param1(@NonNull String param1) {
      this.param1 = param1;
      return this;
    }

    public Builder param1FromRes(@StringRes int stringRes) {
      this.param1 = Res.getString(stringRes);
      return this;
    }

    public Builder param1Size(float param1Size) {
      this.param1Size = param1Size;
      return this;
    }

    public Builder param1Size(int param1Size) {
      this.param1Size = (float) param1Size;
      return this;
    }

    public Builder param1SizeFromRes(@DimenRes int param1SizeRes) {
      this.param1Size = Res.getDimension(param1SizeRes);
      return this;
    }

    public Builder param1Font(String param1Font) {
      this.param1Font = param1Font;
      return this;
    }

    public Builder param1Color(@ColorInt int color) {
      this.param1Color = color;
      return this;
    }

    public Builder param1ColorFromRes(@ColorRes int colorRes) {
      this.param1Color = Res.getColor(colorRes);
      return this;
    }

    public Builder param2(@NonNull String param2) {
      this.param2 = param2;
      return this;
    }

    public Builder param2FromRes(@StringRes int stringRes) {
      this.param2 = Res.getString(stringRes);
      return this;
    }

    public Builder param2Size(float size) {
      this.param2Size = size;
      return this;
    }

    public Builder param2Size(int size) {
      this.param2Size = (float) size;
      return this;
    }

    public Builder param2SizeFromRes(@DimenRes int sizeRes) {
      this.param2Size = Res.getDimension(sizeRes);
      return this;
    }

    public Builder param2Font(String font) {
      this.param2Font = font;
      return this;
    }

    public Builder param2Color(@ColorInt int color) {
      this.param2Color = color;
      return this;
    }

    public Builder param2ColorFromRes(@ColorRes int colorRes) {
      this.param2Color = Res.getColor(colorRes);
      return this;
    }

    public Builder param3(@NonNull String param3) {
      this.param3 = param3;
      return this;
    }

    public Builder param4(@NonNull String param4) {
      this.param4 = param4;
      return this;
    }

    public Builder param5(@NonNull String param5) {
      this.param5 = param5;
      return this;
    }

    public Builder params(@NonNull String param1, @NonNull String param2, @NonNull String param3, @NonNull String param4, @NonNull String param5){
      this.param1 = param1;
      this.param2 = param2;
      this.param3 = param3;
      this.param4 = param4;
      this.param5 = param5;
      return this;
    }

    public Builder setCustomAnimations(@AnimRes int animationOpenEnter,
        @AnimRes int animationOpenExit, @AnimRes int animationCloseEnter,
        @AnimRes int animationCloseExit) {
      this.animationOpenEnter = animationOpenEnter;
      this.animationOpenExit = animationOpenExit;
      this.animationCloseEnter = animationCloseEnter;
      this.animationCloseExit = animationCloseExit;
      return this;
    }

    public Builder backPressToClose(boolean backPressToClose) {
      this.backPressToClose = backPressToClose;
      return this;
    }

    public Builder webViewSupportZoom(boolean webViewSupportZoom) {
      this.webViewSupportZoom = webViewSupportZoom;
      return this;
    }

    public Builder webViewMediaPlaybackRequiresUserGesture(
        boolean webViewMediaPlaybackRequiresUserGesture) {
      this.webViewMediaPlaybackRequiresUserGesture = webViewMediaPlaybackRequiresUserGesture;
      return this;
    }

    public Builder webViewBuiltInZoomControls(boolean webViewBuiltInZoomControls) {
      this.webViewBuiltInZoomControls = webViewBuiltInZoomControls;
      return this;
    }

    public Builder webViewDisplayZoomControls(boolean webViewDisplayZoomControls) {
      this.webViewDisplayZoomControls = webViewDisplayZoomControls;
      return this;
    }

    public Builder webViewAllowFileAccess(boolean webViewAllowFileAccess) {
      this.webViewAllowFileAccess = webViewAllowFileAccess;
      return this;
    }

    public Builder webViewAllowContentAccess(boolean webViewAllowContentAccess) {
      this.webViewAllowContentAccess = webViewAllowContentAccess;
      return this;
    }

    public Builder webViewLoadWithOverviewMode(boolean webViewLoadWithOverviewMode) {
      this.webViewLoadWithOverviewMode = webViewLoadWithOverviewMode;
      return this;
    }

    public Builder webViewSaveFormData(boolean webViewSaveFormData) {
      this.webViewSaveFormData = webViewSaveFormData;
      return this;
    }

    public Builder webViewTextZoom(int webViewTextZoom) {
      this.webViewTextZoom = webViewTextZoom;
      return this;
    }

    public Builder webViewUseWideViewPort(boolean webViewUseWideViewPort) {
      this.webViewUseWideViewPort = webViewUseWideViewPort;
      return this;
    }

    public Builder webViewSupportMultipleWindows(boolean webViewSupportMultipleWindows) {
      this.webViewSupportMultipleWindows = webViewSupportMultipleWindows;
      return this;
    }

    public Builder webViewLayoutAlgorithm(WebSettings.LayoutAlgorithm webViewLayoutAlgorithm) {
      this.webViewLayoutAlgorithm = webViewLayoutAlgorithm;
      return this;
    }

    public Builder webViewStandardFontFamily(String webViewStandardFontFamily) {
      this.webViewStandardFontFamily = webViewStandardFontFamily;
      return this;
    }

    public Builder webViewFixedFontFamily(String webViewFixedFontFamily) {
      this.webViewFixedFontFamily = webViewFixedFontFamily;
      return this;
    }

    public Builder webViewSansSerifFontFamily(String webViewSansSerifFontFamily) {
      this.webViewSansSerifFontFamily = webViewSansSerifFontFamily;
      return this;
    }

    public Builder webViewSerifFontFamily(String webViewSerifFontFamily) {
      this.webViewSerifFontFamily = webViewSerifFontFamily;
      return this;
    }

    public Builder webViewCursiveFontFamily(String webViewCursiveFontFamily) {
      this.webViewCursiveFontFamily = webViewCursiveFontFamily;
      return this;
    }

    public Builder webViewFantasyFontFamily(String webViewFantasyFontFamily) {
      this.webViewFantasyFontFamily = webViewFantasyFontFamily;
      return this;
    }

    public Builder webViewMinimumFontSize(int webViewMinimumFontSize) {
      this.webViewMinimumFontSize = webViewMinimumFontSize;
      return this;
    }

    public Builder webViewMinimumLogicalFontSize(int webViewMinimumLogicalFontSize) {
      this.webViewMinimumLogicalFontSize = webViewMinimumLogicalFontSize;
      return this;
    }

    public Builder webViewDefaultFontSize(int webViewDefaultFontSize) {
      this.webViewDefaultFontSize = webViewDefaultFontSize;
      return this;
    }

    public Builder webViewDefaultFixedFontSize(int webViewDefaultFixedFontSize) {
      this.webViewDefaultFixedFontSize = webViewDefaultFixedFontSize;
      return this;
    }

    public Builder webViewLoadsImagesAutomatically(boolean webViewLoadsImagesAutomatically) {
      this.webViewLoadsImagesAutomatically = webViewLoadsImagesAutomatically;
      return this;
    }

    public Builder webViewBlockNetworkImage(boolean webViewBlockNetworkImage) {
      this.webViewBlockNetworkImage = webViewBlockNetworkImage;
      return this;
    }

    public Builder webViewBlockNetworkLoads(boolean webViewBlockNetworkLoads) {
      this.webViewBlockNetworkLoads = webViewBlockNetworkLoads;
      return this;
    }

    public Builder webViewJavaScriptEnabled(boolean webViewJavaScriptEnabled) {
      this.webViewJavaScriptEnabled = webViewJavaScriptEnabled;
      return this;
    }

    public Builder webViewAllowUniversalAccessFromFileURLs(
        boolean webViewAllowUniversalAccessFromFileURLs) {
      this.webViewAllowUniversalAccessFromFileURLs = webViewAllowUniversalAccessFromFileURLs;
      return this;
    }

    public Builder webViewAllowFileAccessFromFileURLs(boolean webViewAllowFileAccessFromFileURLs) {
      this.webViewAllowFileAccessFromFileURLs = webViewAllowFileAccessFromFileURLs;
      return this;
    }

    public Builder webViewGeolocationDatabasePath(String webViewGeolocationDatabasePath) {
      this.webViewGeolocationDatabasePath = webViewGeolocationDatabasePath;
      return this;
    }

    public Builder webViewAppCacheEnabled(boolean webViewAppCacheEnabled) {
      this.webViewAppCacheEnabled = webViewAppCacheEnabled;
      return this;
    }

    public Builder webViewAppCachePath(String webViewAppCachePath) {
      this.webViewAppCachePath = webViewAppCachePath;
      return this;
    }

    public Builder webViewDatabaseEnabled(boolean webViewDatabaseEnabled) {
      this.webViewDatabaseEnabled = webViewDatabaseEnabled;
      return this;
    }

    public Builder webViewDomStorageEnabled(boolean webViewDomStorageEnabled) {
      this.webViewDomStorageEnabled = webViewDomStorageEnabled;
      return this;
    }

    public Builder webViewGeolocationEnabled(boolean webViewGeolocationEnabled) {
      this.webViewGeolocationEnabled = webViewGeolocationEnabled;
      return this;
    }

    public Builder webViewJavaScriptCanOpenWindowsAutomatically(
        boolean webViewJavaScriptCanOpenWindowsAutomatically) {
      this.webViewJavaScriptCanOpenWindowsAutomatically =
          webViewJavaScriptCanOpenWindowsAutomatically;
      return this;
    }

    public Builder webViewDefaultTextEncodingName(String webViewDefaultTextEncodingName) {
      this.webViewDefaultTextEncodingName = webViewDefaultTextEncodingName;
      return this;
    }

    public Builder webViewUserAgentString(String webViewUserAgentString) {
      this.webViewUserAgentString = webViewUserAgentString;
      return this;
    }

    public Builder webViewNeedInitialFocus(boolean webViewNeedInitialFocus) {
      this.webViewNeedInitialFocus = webViewNeedInitialFocus;
      return this;
    }

    public Builder webViewCacheMode(int webViewCacheMode) {
      this.webViewCacheMode = webViewCacheMode;
      return this;
    }

    public Builder webViewMixedContentMode(int webViewMixedContentMode) {
      this.webViewMixedContentMode = webViewMixedContentMode;
      return this;
    }

    public Builder webViewOffscreenPreRaster(boolean webViewOffscreenPreRaster) {
      this.webViewOffscreenPreRaster = webViewOffscreenPreRaster;
      return this;
    }

    public Builder webViewDesktopMode(boolean webViewDesktopMode) {
      return webViewUserAgentString(
          "Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.9.0.4) Gecko/20100101 Firefox/4.0");
    }

    public Builder injectJavaScript(String injectJavaScript) {
      this.injectJavaScript = injectJavaScript;
      return this;
    }

    public void load(){
      load("<!DOCTYPE html><html><head><meta content='text/html; charset=UTF-8' http-equiv='Content-Type'><meta content='IE=edge' http-equiv='X-UA-Compatible'><meta charset='utf-8'><meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'><title>Survey Millions | Pollfish.com</title><link href='images/favicon.ico' rel='shortcut icon'><meta content='//www.pollfish.com' property='og:url'><meta content='Survey Millions | Pollfish.com' property='og:title'><meta content='Looking for mobile friendly online survey software &amp;amp; tools? With Pollfish, you get exactly that. Sign-up today &amp;amp; create your first survey.' name='description'><meta content='https://storage.googleapis.com/pollfish-images/A_LP/home_meta.png' property='og:image'><meta content='https://storage.googleapis.com/pollfish-images/A_LP/home_meta.png' name='twitter:image'><meta content='summary_large_image' name='twitter:card'><meta content='noodp' name='robots'><meta content='en_US' property='og:locale'><link href=\"site.css\" rel=\"stylesheet\"/></head><body class='home'><div id='cookie-banner'></div><div class='user-ribbon d-flex align-items-center'><div class='container'><a href='/publisher'> Publisher's area <span class='icon-keyboard_arrow_right'></span></a></div></div><header class='header'><div class='container header__container'><div class='header__logo'><a href=\"/\" class=\"logo\"><span class='icon-logo header__logo-icon'></span></a></div><a href=\"#\" class=\"js-mobile-link mobile-link-btn\"><span></span><span></span><span></span><span></span></a><div class='header__links'><ul class='header__menu-list list-inline mb-0'><li class='list-inline-item list-inline-item--with-dropdown' data-sub='how-it-works'><a href=\"\" class=\"header__ahref-with-dropdown\">How it works</a></li><li class='list-inline-item list-inline-item--with-dropdown' data-sub='product-highlights'><a href=\"\" class=\"header__ahref-with-dropdown\">Features</a></li><li class='list-inline-item'><a href=\"/pricing\">Pricing</a></li><li class='list-inline-item'><a href=\"//help.pollfish.com\" target=\"_blank\">Resources</a></li><li class='list-inline-item'><a href=\"//www.pollfish.com/blog/\" target=\"_blank\">Blog</a></li></ul><div class='header__actions'><a href=\"//www.pollfish.com/login/researcher\" class=\"pf-btn pf-btn--xl pf-btn--flat mr-2\">Log in</a><a href=\"//www.pollfish.com/signup/researcher\" class=\"pf-btn pf-btn--xl pf-btn--primary\">Sign up</a></div></div></div><div class='dropdown-holder'><div class='dropdown__arrow'></div><div class='dropdown__bg'><div class='dropdown__bg-bottom'></div></div><div class='dropdown__wrap'><div class='how-it-works dropdown-menu' data-sub='how-it-works'><div class='dropdown-menu__content'><div class='top-section'><div class='row'><div class='col-md-12'><span class='header__section-title'>How it works</span><ul class='top-section__list'><li><a class='d-flex align-items-center' href='/audience'><div class='dropdown-menu__icon-container'><span class='icon-location_searching pf-text-color--text-link dropdown-menu__icon--default'></span><span class='icon-keyboard_arrow_right pf-text-color--text-link dropdown-menu__icon--on-hover'></span></div><div class='dropdown-menu__details-containers'><h3 class='pf-margin--bottom-0 pf-margin--top-0'>Audience</h3></div></a></li><li><a class='d-flex align-items-center' href='/questionnaire'><div class='dropdown-menu__icon-container'><span class='icon-subject pf-text-color--text-link dropdown-menu__icon--default'></span><span class='icon-keyboard_arrow_right pf-text-color--text-link dropdown-menu__icon--on-hover'></span></div><div class='dropdown-menu__details-containers'><h3 class='pf-margin--bottom-0 pf-margin--top-0'>Questionnaire</h3></div></a></li><li><a class='d-flex align-items-center' href='/results'><div class='dropdown-menu__icon-container'><span class='icon-show_chart pf-text-color--text-link dropdown-menu__icon--default'></span><span class='icon-keyboard_arrow_right pf-text-color--text-link dropdown-menu__icon--on-hover'></span></div><div class='dropdown-menu__details-containers'><h3 class='pf-margin--bottom-0 pf-margin--top-0'>Results</h3></div></a></li></ul></div></div></div></div></div><div class='product-highlights dropdown-menu' data-sub='product-highlights'><div class='dropdown-menu__content'><div class='top-section'><div class='row'><div class='col-xl-6'><span class='header__section-title'>Features</span><ul class='top-section__list top-section__list--with-border'><li><a href='//www.pollfish.com/lp/pollfish-personas'><div class='dropdown-menu__icon-container'><span class='icon-person_pin pf-text-color--cornflower-blue dropdown-menu__icon--default'></span><span class='icon-keyboard_arrow_right pf-text-color--cornflower-blue dropdown-menu__icon--on-hover'></span></div><div class='dropdown-menu__details-containers'><h3>Personas</h3><p> Get even more insights on the users that took your survey and their responses. </p></div></a></li><li><a href='//www.pollfish.com/lp/nps'><div class='dropdown-menu__icon-container'><span class='icon-network_check pf-text-color--shakespeare dropdown-menu__icon--default'></span><span class='icon-keyboard_arrow_right pf-text-color--shakespeare dropdown-menu__icon--on-hover'></span></div><div class='dropdown-menu__details-containers'><h3>Net Promoter Score</h3><p>Measure the loyalty of your customers.</p></div></a></li></ul></div><div class='col-xl-6'><ul class='top-section__list'><li><a href='//www.pollfish.com/lp/logo-poll'><div class='dropdown-menu__icon-container'><span class='icon-lightbulb_outline pf-text-color--razzmatazz dropdown-menu__icon--default'></span><span class='icon-keyboard_arrow_right pf-text-color--razzmatazz dropdown-menu__icon--on-hover'></span></div><div class='dropdown-menu__details-containers'><h3>Concept Testing</h3><p> Receive instant feedback on your logo/website/campaign or concept </p></div></a></li><li><a href='//www.pollfish.com/lp/behind-enemy-lines'><div class='dropdown-menu__icon-container'><span class='icon-get_app dropdown-menu__icon--default pf-text-color--willow-grove'></span><span class='icon-keyboard_arrow_right pf-text-color--willow-grove dropdown-menu__icon--on-hover'></span></div><div class='dropdown-menu__details-containers'><h3>Installed app</h3><p>Αsk your competitor's users questions directly.</p></div></a></li></ul></div></div></div></div></div><ul class='header__mobile-links'><li><a href=\"/pricing\">Pricing</a></li><li><a href=\"//help.pollfish.com\" target=\"_blank\">Resources</a></li><li><a href=\"//www.pollfish.com/blog/\" target=\"_blank\">Blog</a></li></ul><ul class='header__mobile-action-links'><li><a href=\"//www.pollfish.com/login/researcher\" class=\"\">Log in</a></li><li><a href=\"//www.pollfish.com/signup/researcher\" class=\"\">Sign up</a></li></ul></div></div></header><main><section class='pf-hero'><div class='container pf-padding--bottom-12'><div class='row'><div class='col-12 col-lg-8 col-xl-4 mb-5 mb-lg-0'><h1> Survey millions of consumers anytime, anywhere </h1><div class='dots pf-padding--bottom-8'><div class='dot'></div><div class='dot'></div><div class='dot'></div></div><p class='pf-padding--bottom-10'> Pollfish is a survey platform that delivers surveys online and via mobile apps on a global scale. Reach your target audience and collect cost-effective and fast survey results. </p><div class='pf-padding--bottom-10'><a href=\"//www.pollfish.com/signup/researcher\" class=\"pf-btn pf-btn--xl pf-btn--primary\">Create a survey</a></div></div><div class='col-12 col-lg-12 col-xl-7 offset-xl-1 d-flex align-items-center justify-content-center map-container'><div class='map'></div><div class='users-counter'><div class='users-counter__item' id='users-counter-item'></div><div class='loader'><div class='loader__anim'></div></div><div class='users-counter__text'> Consumers to survey instantly </div></div></div></div></div></section><section class='pollfish-how-it-works pf-padding--bottom-40'><div class='container home-steps'><div class='pf-card pf-card--lg pf-color--white pf-box-shadow--4dp'><h2 class='pf-margin--bottom-10 pf-margin--top-0'>One stop service</h2><div class='row'><div class='col-12 col-lg-4 home-steps__col'><div class='home-steps__number'> 1 </div><h3 class='pf-margin--bottom-4 home-steps__title'>Select your target audience</h3><div class='home-steps__content'><p class='pf-margin--bottom-4'> Select your screening criteria such as age, gender and location to reach millions of mobile consumers. </p><a class='d-flex align-items-center pf-margin--bottom-8' href='/audience'> Learn more <span class='icon-keyboard_arrow_right pf-font-size--24'></span></a></div><div class='pollfish-how-it-works__step pollfish-how-it-works--step1'></div></div><div class='col-12 col-lg-4 home-steps__col'><div class='home-steps__number'> 2 </div><h3 class='pf-margin--bottom-4 home-steps__title'>Create your survey</h3><div class='home-steps__content'><p class='pf-margin--bottom-4'> Design your survey questions using features like survey skip logic, randomization of answers, open-ended, image, video and NPS question types. </p><a class='d-flex align-items-center pf-margin--bottom-8' href='/questionnaire'> Learn more <span class='icon-keyboard_arrow_right pf-font-size--24'></span></a></div><div class='pollfish-how-it-works__step pollfish-how-it-works--step2'></div></div><div class='col-12 col-lg-4'><div class='home-steps__number'> 3 </div><h3 class='pf-margin--bottom-4 home-steps__title'>We collect responses. Fast</h3><div class='home-steps__content'><p class='pf-margin--bottom-4'> Leverage the power of the Pollfish mobile survey network to get results in real-time. </p><a class='d-flex align-items-center pf-margin--bottom-8' href='/results'> Learn more <span class='icon-keyboard_arrow_right pf-font-size--24'></span></a></div><div class='pollfish-how-it-works__step pollfish-how-it-works--step3'></div></div></div></div></div></section><section class='pollfish-video'><div class='container'><a class='pollfish-video__href' data-toggle='modal' href='#videoModal'></a></div></section><section class='pollfish-demographics-info pf-padding--top-14 pf-padding--bottom-40'><div class='container'><div class='row'><div class='col-12 col-lg-5 col-xl-4 mb-5 mb-lg-0'><img class='pf-icon pf-icon--lg pf-margin--bottom-10' src='images/svg/demographic.svg'><h2 class='pf-margin--bottom-6 pf-margin--top-0'> Demographic information takes your results a step further – for <span class='pf-text-color--primary'>FREE</span></h2><p> Who is that respondent that gave answer A to Question 1 of your survey? We know. And we will tell you, for free. </p><p class='pf-margin--bottom-8'> When your survey completes, along with the answers to your survey questions, you will receive all the demographic information we keep for each respondent. What their chosen career is, whether they are married or not and much more.You can then use all this valuable insight to design more successful campaigns and better targeted advertisements. </p><a class='d-flex align-items-center' href='//www.pollfish.com/dashboard/results/372130/-95234720' target='_blank'> See sample results <span class='icon-keyboard_arrow_right pf-font-size--24'></span></a></div><div class='col-12 col-lg-7 col-xl-7 offset-xl-1 d-flex align-items-center'><div class='pollfish-demographics-info__image'></div></div></div></div></section><section class='pollfish-support pf-padding--top-40 pf-padding--bottom-40'><div class='container'><div class='row'><div class='col-12 col-lg-5 col-xl-4 pf-margin--bottom-6'><img class='pf-icon pf-icon--lg pf-margin--bottom-10' src='images/svg/support.svg'><h2 class='pf-margin--bottom-6 pf-margin--top-0'> You are never alone </h2><p> Even when you have done all your work and are ready to run your survey, our work is not finished. Our team of experts reviews all surveys that have been submitted, and will examine your survey’s methodology and design. The final decision on whether to make the changes or not lies with you. </p><p class='pf-margin--bottom-8'> Once your survey is approved, you will receive via email a link to your online results. And if there is anything you would like to ask, our support team is only a few clicks away, available 24/7. </p><div class='d-flex align-items-center live-support'><div class='pf-margin--right-3'><span class='icon-support pf-font-size--32'></span></div> 24/7 Live support </div></div><div class='col-12 col-lg-7 col-xl-7 offset-xl-1 d-flex align-items-center'><div class='pollfish-support__image'></div></div></div></div></section><section class='pollfish-pricing pf-padding--bottom-40 pf-padding--top-40'><div class='container pf-padding--bottom-20'><div class='row'><div class='col-12 col-lg-5 mb-5 mb-lg-0'><?xml version=\"1.0\" encoding=\"UTF-8\"?><svg width=\"86px\" height=\"80px\" viewbox=\"0 0 86 80\" version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" class=\"pf-icon pf-icon--lg pf-margin--bottom-10\"><title>simplepricing</title><desc>Created with Sketch.</desc><defs><lineargradient x1=\"100%\" y1=\"49.9403697%\" x2=\"1.37862386e-14%\" y2=\"49.9403697%\" id=\"linearGradient-1\"><stop stop-color=\"#EEF4FB\" offset=\"0%\"></stop><stop stop-color=\"#EEF4FB\" offset=\"100%\"></stop></lineargradient><lineargradient x1=\"-7.54491018%\" y1=\"-63.3847906%\" x2=\"79.001996%\" y2=\"101.457166%\" id=\"linearGradient-2\"><stop stop-color=\"#AFF1B6\" offset=\"0%\"></stop><stop stop-color=\"#24B47E\" offset=\"100%\"></stop></lineargradient></defs><g id=\"Page-1\" stroke=\"none\" stroke-width=\"1\" fill=\"none\" fill-rule=\"evenodd\"><g id=\"simplepricing\" transform=\"translate(0.000000, -1.000000)\" fill-rule=\"nonzero\"><g id=\"Group\" transform=\"translate(0.000000, 41.000000)\"><path d=\"M44.12,11.86 C49.42,11.86 51.92,1.86 59.78,1.86 C67.08,1.86 70.3,11.86 74.94,11.86 L85.4,11.86 C85.21,15.22 80.86,17.86 79.45,20.86 C70.79,20.86 51.34,35.4 44.77,35.4 C39.47,35.36 32.35,29 24.5,29 C17.2,29 14,21.36 9.34,21.36 L10.73,21.36 C9.49206739,18.3004194 8.60636805,15.1098888 8.09,11.85 L44.12,11.86 Z\" id=\"Shape\" fill=\"url(#linearGradient-1)\"></path><g id=\"Shape\"><path d=\"M44.29,39.91 C62.6234233,39.9083322 79.1229293,28.7847142 86,11.79 L78,11.79 C73.24,11.79 69.93,19.68 62.43,19.68 C54.36,19.68 49.22,11.68 46.35,11.79 L2.57,11.79 C9.44843781,28.7880928 25.9529321,39.9124068 44.29,39.91\" fill=\"#CEF4D0\"></path><path d=\"M51.61,39.31 C60.4447438,37.8605143 68.6433193,33.7995377 75.15,27.65 C65.0589833,22.6466473 55.2989822,17.0017459 45.93,10.75 C30.15,0.39 14.13,-1 0,3 C0.633048287,6.32592463 1.63941636,9.56978437 3,12.67 C12.9520031,11.3395885 23.0548821,13.5520269 31.54,18.92 C42.63,26.1 48.48,33.92 51.64,39.36\" fill=\"url(#linearGradient-2)\"></path></g></g><g id=\"Group\" transform=\"translate(22.000000, 0.000000)\"><path d=\"M38.9,46.17 L1.47,30.47 C0.347238037,29.9966712 -0.180603357,28.7039071 0.29,27.58 L10.87,2.35 C11.3433288,1.22723804 12.6360929,0.699396643 13.76,1.17 L51.19,16.88 C52.312762,17.3533288 52.8406034,18.6460929 52.37,19.77 L41.79,45 C41.3128048,46.1188689 40.0211616,46.6417833 38.9,46.17 Z M16.18,30.17 C16.305935,29.8730876 16.3081245,29.5381854 16.1860825,29.2396518 C16.0640404,28.9411182 15.8278671,28.70366 15.53,28.58 L6.58,24.83 C6.28308762,24.704065 5.94818544,24.7018755 5.64965182,24.8239175 C5.35111821,24.9459596 5.11365998,25.1821329 4.99,25.48 L4.52,26.6 C4.39406503,26.8969124 4.3918755,27.2318146 4.51391754,27.5303482 C4.63595958,27.8288818 4.87213294,28.06634 5.17,28.19 L14.12,31.94 C14.4169124,32.065935 14.7518146,32.0681245 15.0503482,31.9460825 C15.3488818,31.8240404 15.58634,31.5878671 15.71,31.29 L16.18,30.17 Z M30.72,36.27 C30.845935,35.9730876 30.8481245,35.6381854 30.7260825,35.3396518 C30.6040404,35.0411182 30.3678671,34.80366 30.07,34.68 L21.12,30.93 C20.8230876,30.804065 20.4881854,30.8018755 20.1896518,30.9239175 C19.8911182,31.0459596 19.65366,31.2821329 19.53,31.58 L19.06,32.7 C18.934065,32.9969124 18.9318755,33.3318146 19.0539175,33.6303482 C19.1759596,33.9288818 19.4121329,34.16634 19.71,34.29 L28.66,38.04 C28.9569124,38.165935 29.2918146,38.1681245 29.5903482,38.0460825 C29.8888818,37.9240404 30.12634,37.6878671 30.25,37.39 L30.72,36.27 Z\" id=\"Shape\" fill=\"#DCE9FF\"></path><polygon id=\"Shape\" fill=\"#0E68F9\" points=\"9.68 5.2 51.18 22.61 48.79 28.31 7.29 10.9\"></polygon></g></g></g></svg><h2 class='pf-margin--bottom-6 pf-margin--top-0'> Simple pricing no surprises </h2><p> Our clear and simple cost structure offers predictive pricing for our clients. Prices start at $1 per completed survey, for a maximum of 17 questions. The only additional charges are $0.50 each for adding a filtering question, gender quota, or for selecting an age range. </p><p class='pf-margin--bottom-6'> You only pay per completed survey, not per respondent. </p><a class='d-flex align-items-center' href='/pricing'> Get an instant quote <span class='icon-keyboard_arrow_right pf-font-size--24'></span></a></div><div class='col-12 col-lg-5 offset-lg-1'><?xml version=\"1.0\" encoding=\"UTF-8\"?><svg width=\"86px\" height=\"84px\" viewbox=\"0 0 86 84\" version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" class=\"pf-icon pf-icon--lg pf-margin--bottom-10\"><title>globalscale</title><desc>Created with Sketch.</desc><defs><lineargradient x1=\"100%\" y1=\"49.9403697%\" x2=\"0.0129332644%\" y2=\"49.9403697%\" id=\"linearGradient-1\"><stop stop-color=\"#EEF4FB\" offset=\"0%\"></stop><stop stop-color=\"#EEF4FB\" offset=\"100%\"></stop></lineargradient><lineargradient x1=\"-7.54491018%\" y1=\"-63.415145%\" x2=\"79.001996%\" y2=\"101.588831%\" id=\"linearGradient-2\"><stop stop-color=\"#B28DED\" offset=\"0%\"></stop><stop stop-color=\"#70A6FB\" offset=\"100%\"></stop></lineargradient><radialgradient cx=\"50%\" cy=\"50%\" fx=\"50%\" fy=\"50%\" r=\"50%\" id=\"radialGradient-3\"><stop stop-color=\"#E4F0FA\" offset=\"0%\"></stop><stop stop-color=\"#BDD7F9\" offset=\"100%\"></stop><stop stop-color=\"#EEF4FB\" offset=\"100%\"></stop></radialgradient><radialgradient cx=\"49.9942534%\" cy=\"49.9606373%\" fx=\"49.9942534%\" fy=\"49.9606373%\" r=\"36.8254553%\" gradienttransform=\"translate(0.499943,0.499606),scale(0.290561,1.000000),translate(-0.499943,-0.499606)\" id=\"radialGradient-4\"><stop stop-color=\"#AFF1B6\" offset=\"0%\"></stop><stop stop-color=\"#24B47E\" stop-opacity=\"0.2\" offset=\"100%\"></stop></radialgradient></defs><g id=\"Page-1\" stroke=\"none\" stroke-width=\"1\" fill=\"none\" fill-rule=\"evenodd\"><g id=\"globalscale\" fill-rule=\"nonzero\"><g id=\"Group\" transform=\"translate(0.000000, 44.000000)\"><path d=\"M44.12,11.93 C49.42,11.93 51.92,1.93 59.78,1.93 C67.08,1.93 70.3,11.93 74.94,11.93 L85.4,11.93 C85.21,15.29 80.86,17.93 79.45,20.93 C70.79,20.93 51.34,35.47 44.77,35.47 C39.47,35.47 32.35,29.15 24.49,29.15 C17.19,29.15 13.97,21.47 9.33,21.47 L10.72,21.47 C9.48206739,18.4104194 8.59636805,15.2198888 8.08,11.96 L44.12,11.93 Z\" id=\"Shape\" fill=\"url(#linearGradient-1)\"></path><g id=\"Shape\"><path d=\"M44.29,40 C62.6283381,39.9950357 79.1292364,28.8625668 86,11.86 L78,11.86 C73.24,11.86 69.93,19.75 62.43,19.75 C54.36,19.75 49.22,11.75 46.35,11.86 L2.57,11.86 C9.44212968,28.8659455 25.9480168,39.999111 44.29,40\" fill=\"#BDD7F9\"></path><path d=\"M51.61,39.38 C60.4458859,37.927702 68.6445858,33.8631809 75.15,27.71 C65.0589833,22.7066473 55.2989822,17.0617459 45.93,10.81 C30.15,0.46 14.13,-0.91 0,3 C0.633048287,6.32592463 1.63941636,9.56978437 3,12.67 C12.9520031,11.3395885 23.0548821,13.5520269 31.54,18.92 C42.63,26.1 48.48,33.92 51.64,39.36\" fill=\"url(#linearGradient-2)\"></path></g></g><g id=\"Group\" transform=\"translate(2.000000, 0.000000)\"><circle id=\"Oval\" fill=\"url(#radialGradient-3)\" cx=\"42.37\" cy=\"22.14\" r=\"22.14\"></circle><path d=\"M59.66,8.31 C59.2718162,8.62272744 58.7181838,8.62272744 58.33,8.31 C58.6933226,7.79593278 58.6430884,7.09684008 58.21,6.64 L57.79,6.22 C56.8781429,5.57543743 55.683746,5.49067378 54.69,6 C54.3255661,6.20956991 53.9894949,6.46498404 53.69,6.76 C52.8511718,7.38598541 52.2911179,8.31581844 52.13,9.35 C52.0826361,9.97457162 52.3306128,10.5852606 52.8,11 C53.2908726,11.3973084 53.9711797,11.4641594 54.53,11.17 C54.7676222,11.0528821 54.9295563,10.8231617 54.96,10.56 C54.96,10.27 54.75,10.04 54.64,9.78 C54.5096403,9.36820073 54.6106537,8.9181153 54.9044884,8.60151868 C55.1983232,8.28492205 55.639636,8.15067009 56.06,8.25 C55.5490988,8.75608809 55.2550403,9.44102923 55.24,10.16 L56.87,10.22 C56.97,10.42 56.78,10.64 56.59,10.74 C56.3799573,10.7924216 56.1804183,10.880354 56,11 C55.7,11.31 55.88,11.87 55.61,12.21 C55.34,12.55 54.61,12.34 54.09,12.1 C53.31,11.73 52.21,11.68 51.76,12.42 C51.56,12.75 51.49,13.23 51.12,13.35 C50.850672,13.3839763 50.577516,13.3314463 50.34,13.2 C50.08,13.11 49.73,13.13 49.63,13.38 C49.53,13.63 49.74,13.85 49.89,14.04 C50.1602128,14.3555717 50.2933868,14.7658916 50.26,15.18 C50.2143233,15.6014346 49.8894404,15.9386548 49.47,16 C49.16,16 48.84,15.86 48.56,16 C48.4163262,16.0932498 48.3176299,16.2417816 48.2873234,16.4103615 C48.2570169,16.5789414 48.2978006,16.7525483 48.4,16.89 C48.6206725,17.1555853 48.9294318,17.3330332 49.27,17.39 C50.21,17.63 51.48,17.39 51.67,16.39 C51.67,16.2 51.67,15.97 51.85,15.84 C51.9650161,15.7519157 52.1051576,15.7028662 52.25,15.7 C52.7051826,15.5798965 53.1902599,15.6717672 53.57,15.95 C53.7068666,16.1106778 53.8090076,16.2979363 53.87,16.5 L54.66,18.58 L54.28,15.8 C54.96,16.8 55.8,18 57.05,18.14 C56.58,18 56.45,17.37 56.61,16.9 C56.77,16.43 57.11,16.05 57.28,15.59 C57.3242553,15.4015147 57.41346,15.2265362 57.54,15.08 C57.8086144,14.9224968 58.1413856,14.9224968 58.41,15.08 C59.1416224,15.3623839 59.8059121,15.7950239 60.36,16.35 C59.5914094,16.1401583 58.7965289,16.0424823 58,16.06 C57.7163575,16.042256 57.4358398,16.1274765 57.21,16.3 C56.8,16.69 57.09,17.41 57.55,17.75 C58.0494829,18.0104504 58.5247044,18.3149937 58.97,18.66 C59.0720442,18.7551968 59.1389874,18.8820365 59.16,19.02 C59.16,19.35 58.78,19.57 58.45,19.53 C58.1307913,19.4449174 57.8221798,19.3241564 57.53,19.17 C57.2243139,19.0186083 56.8557126,19.0834173 56.62,19.33 C56.8327662,19.6166733 56.9161422,19.9791778 56.85,20.33 C56.1994225,20.4194996 55.5371237,20.364877 54.91,20.17 C54.2713978,19.9607395 53.8266756,19.3810124 53.79,18.71 C53.79,18.13 54.18,17.41 53.71,17.07 C53.572544,16.9846796 53.4111841,16.9460935 53.25,16.96 C52.57,16.96 52,17.51 51.33,17.66 C51.04,17.72 50.75,17.72 50.46,17.78 C49.9988803,17.9131683 49.5677035,18.1338494 49.19,18.43 C48.3562237,18.875768 47.6869327,19.5765963 47.28,20.43 C47.1232023,21.1293155 47.0994554,21.8518986 47.21,22.56 C47.1885394,23.5632268 47.248738,24.5665366 47.39,25.56 C47.5476051,26.5669761 48.2473468,27.4051282 49.21,27.74 C50.3215734,27.0611541 51.6646295,26.8729091 52.92,27.22 C52.5,27.77 52.92,28.54 53.45,29.04 C53.98,29.54 54.5,30.18 54.24,30.82 C54.07,31.25 53.53,31.58 53.63,32.04 C53.6974594,32.236929 53.810537,32.4151118 53.96,32.56 C55.21,34.11 53.77,36.56 54.58,38.37 C55.342406,38.6208544 56.1742259,38.5491054 56.8824156,38.1714042 C57.5906053,37.7937031 58.1135906,37.1428893 58.33,36.37 C58.3645412,36.0958256 58.4213965,35.8249268 58.5,35.56 C58.76,34.88 59.5,34.56 59.93,33.95 C60.93,32.55 59.75,30.45 60.56,28.95 C60.8019764,28.5717433 61.1023687,28.2342239 61.45,27.95 L63.32,26.18 C62.5640283,26.3320225 61.778919,26.2264024 61.09,25.88 C60.5143231,25.4365428 60.0743702,24.8407011 59.82,24.16 C59.1803493,23.008217 58.7375898,21.7576748 58.51,20.46 C58.99,20.17 59.51,20.7 59.85,21.17 C60.574871,22.0428514 60.9648759,23.1455018 60.95,24.28 C60.95,24.47 60.88,24.7 61.02,24.83 C61.1025627,24.8964799 61.2041171,24.9350006 61.31,24.94 C62.2279822,25.0513348 63.1379159,24.6779362 63.7130863,23.9538742 C64.2882567,23.2298122 64.4461219,22.2589958 64.13,21.39 L64.52,21.46 C64.3773379,16.6639706 62.6706477,12.0460743 59.66,8.31 L59.66,8.31 Z\" id=\"Shape\" fill-opacity=\"0.6\" fill=\"#FFFFFF\"></path><path d=\"M48.34,0.82 C38.1832888,-2.01301852 27.4365125,2.69967287 22.64,12.09 C22.7244078,12.4468258 22.7580727,12.8137723 22.74,13.18 C22.67,14.11 21.94,14.73 21.74,15.63 C21.5961435,16.3599794 21.6763167,17.1163958 21.97,17.8 C22.23,18.49 22.58,19.15 22.8,19.86 C22.9259256,20.7621579 23.2959097,21.6127755 23.87,22.32 C24.2765359,22.6150062 24.7342846,22.832013 25.22,22.96 C26.4641951,23.3319403 27.6252712,23.939638 28.64,24.75 L30.19,26.15 C30.606088,26.4355213 30.8849533,26.8809814 30.96,27.38 C30.9098204,27.7301829 30.7980996,28.0687307 30.63,28.38 C30.2,29.55 30.8,30.83 31.39,31.94 L32.75,34.5 C33.0657295,35.0436836 33.3206002,35.6204964 33.51,36.22 C33.6701433,36.8738988 33.7341491,37.5476435 33.7,38.22 C33.6653543,39.1535452 33.5583669,40.0829982 33.38,41 C33.2836113,41.419489 33.2432597,41.8499051 33.26,42.28 L33.88,42.55 C34.0240441,42.1105892 34.2403429,41.6982696 34.52,41.33 C34.9391204,40.6635412 35.4359354,40.0492362 36,39.5 C36.38,39.16 36.82,38.88 37.21,38.5 C37.8153724,37.9786026 38.3028384,37.3343294 38.64,36.61 C38.6898404,36.4748337 38.7682971,36.3520319 38.87,36.25 C38.9782035,36.1733232 39.1005778,36.1189346 39.23,36.09 C39.7686121,35.9878928 40.2597344,35.7142675 40.63,35.31 C40.9140695,34.8029998 41.0621983,34.2311539 41.06,33.65 C41.1652915,32.7887453 41.4297855,31.9545718 41.84,31.19 C42.03,30.84 42.24,30.38 41.99,30.08 C41.8327537,29.9345168 41.6329444,29.8433757 41.42,29.82 C40.88,29.71 40.51,29.58 39.97,29.47 C39.2308231,28.5858696 38.3726562,27.8084714 37.42,27.16 L35.59,25.79 C35.406704,25.6336354 35.1966634,25.5116763 34.97,25.43 C34.7833785,25.3861 34.5914144,25.3692611 34.4,25.38 C33.8266667,25.38 33.2566667,25.4 32.69,25.44 C32.4440953,25.4303531 32.2007484,25.4929281 31.99,25.62 C31.87,25.71 31.79,25.83 31.67,25.92 C31.5415578,26.0004081 31.3912762,26.0388522 31.24,26.03 C30.6449779,25.9669379 30.2046129,25.4473072 30.24,24.85 C30.24,24.62 30.34,24.34 30.17,24.19 C30,24.04 29.78,24.14 29.6,24.06 C29.42,23.98 29.33,23.47 29.33,23.12 C29.3240087,22.7182946 29.1751081,22.3318622 28.91,22.03 C28.6698374,22.5131378 28.1887064,22.8300733 27.65,22.86 C27.1035166,22.8654027 26.626091,22.4917653 26.5,21.96 C26.4673387,21.5731042 26.5546286,21.185537 26.75,20.85 C26.9889974,20.2542675 27.3825131,19.7330318 27.89,19.34 C28.4254083,18.9576345 29.1445917,18.9576345 29.68,19.34 C30.24,19.8 30.27,20.67 30.79,21.17 C30.8715365,20.80548 30.9151023,20.4334957 30.92,20.06 C31.1218967,19.1815459 31.577425,18.3817634 32.23,17.76 C32.73,17.18 33.23,16.27 33.87,15.92 C34.6458778,15.5163708 35.296146,14.9076091 35.75,14.16 C36.07,13.63 36.35,12.98 36.94,12.79 C37.0041484,12.7769891 37.0631778,12.7457382 37.11,12.7 C37.1337158,12.6354425 37.1337158,12.5645575 37.11,12.5 C37.11,11.8 36.98,10.96 36.34,10.65 C36.03,10.5 35.64,10.52 35.34,10.3 C35.4764928,9.95110182 35.385207,9.55420723 35.11,9.3 C34.8251018,9.05976268 34.4787484,8.90390364 34.11,8.85 C34.0028029,8.81488212 33.8871971,8.81488212 33.78,8.85 C33.6940727,8.90029484 33.6272514,8.97766684 33.59,9.07 C33.4407768,9.52498085 33.3664753,10.0011861 33.37,10.48 C33.2060768,11.2517957 32.8635894,11.9744442 32.37,12.59 C32.3237649,12.6694124 32.253827,12.7323564 32.17,12.77 C31.94,12.84 31.78,12.53 31.79,12.29 C31.8,12.05 31.87,11.76 31.69,11.6 C31.5880079,11.5243786 31.4666278,11.4792946 31.34,11.47 C30.51,11.32 29.58,10.86 29.48,10.02 C29.4908376,9.45769276 29.7038351,8.91809912 30.08,8.5 C30.3,8.18 30.58,7.84 30.97,7.83 C31.36,7.82 31.74,8.22 32.14,8.14 C32.2681052,7.29726919 32.6360896,6.50922705 33.2,5.87 C33.3727922,5.64195771 33.6208023,5.4825226 33.9,5.42 C34.0385723,5.39720382 34.1804123,5.43255705 34.2920752,5.51772364 C34.403738,5.60289022 34.4753408,5.73033236 34.49,5.87 C34.4602718,6.04262624 34.3802641,6.20264171 34.26,6.33 C34.017138,6.66370036 33.8592503,7.05155506 33.8,7.46 C34.75,7.94 35.87,8.46 36.8,7.94 C36.8209912,7.09225734 36.4525126,6.28160457 35.8,5.74 C35.23,5.26 34.41,4.93 34.22,4.21 C34.1449976,3.84659538 34.2092406,3.46827525 34.4,3.15 C34.7840613,2.45158774 35.5130544,2.01266523 36.31,2 C36.3181399,2.24674744 36.4653639,2.46758338 36.69,2.57 C36.9120913,2.66615107 37.1432414,2.73985111 37.38,2.79 C38.5278723,3.22502831 39.3781225,4.21071662 39.64,5.41 C39.8811285,6.59088864 39.9351209,7.80234259 39.8,9 C39.8,9.14 39.8,9.31 39.91,9.39 C39.9742769,9.42512624 40.0467904,9.44239137 40.12,9.44 C40.3697618,9.4554341 40.6202382,9.4554341 40.87,9.44 C40.9628444,9.44079462 41.0536618,9.41285081 41.13,9.36 C41.205615,9.28019335 41.2545407,9.17884732 41.27,9.07 C41.4646883,8.3799541 41.9106098,7.78777039 42.52,7.41 C43.1290002,7.03645988 43.8109793,6.79776721 44.52,6.71 C44.7131898,6.69502416 44.902491,6.64769887 45.08,6.57 C45.7,6.24 45.65,5.24 46.23,4.84 C46.3451523,4.77984631 46.4526004,4.70597578 46.55,4.62 C46.6696713,4.43687871 46.7193719,4.21677622 46.69,4 C46.6865539,3.28748738 46.8437851,2.58336502 47.15,1.94 C47.31,1.61 47.64,1.24 47.98,1.38 C48.061908,1.41549523 48.1549195,1.41516323 48.2365721,1.3790842 C48.3182246,1.34300517 48.381095,1.2744591 48.41,1.19 C48.4337109,1.06225265 48.4087387,0.930256885 48.34,0.82 L48.34,0.82 Z\" id=\"Shape\" fill-opacity=\"0.6\" fill=\"#FFFFFF\"></path><path d=\"M81.9,14 C81.34,10.1 72.28,7.94 59.38,8 C60.1684039,8.93965963 60.8775019,9.94310023 61.5,11 C67.84,11.56 71.98,13.06 72.3,15.35 C72.93,19.72 59.43,25.28 42.14,27.77 C24.85,30.26 10.33,28.77 9.7,24.38 C9.33,21.83 13.79,18.87 21,16.38 C21.3076877,15.2466742 21.7056259,14.1398041 22.19,13.07 C8.58,16.71 -0.49,21.65 0.11,25.78 C0.93,31.49 19.91,33.47 42.5,30.21 C65.09,26.95 82.72,19.68 81.9,14 Z\" id=\"Shape\" fill=\"url(#radialGradient-4)\"></path></g></g></g></svg><h2 class='pf-margin--bottom-6 pf-margin--top-0'> Global scale </h2><p class='pf-margin--bottom-6'> Our platform gives researchers the ability to create surveys and collect results online. Assisted by a well-trained support team and a tech team that uses the latest techniques to provide significant insights, we are trying to democratize Market Research by making it affordable and easy to use. </p></div></div></div></section><section class='pf-color--dark'><div class='container'><div class='pf-card pf-card--lg pf-color--white pf-box-shadow--4dp pf-card--relative-on-top-80 d-lg-flex justify-content-between align-items-center text-center text-sm-left'><div class='mb-4 mb-lg-0'><h2 class='pf-margin--top-0 pf-margin--bottom-1'>Ready to get started?</h2><p class='pf-margin--bottom-0'> Get in touch, or create an account. </p></div><div><a href=\"#contact-sales-modal\" data-toggle=\"modal\" class=\"pf-btn pf-btn--xl pf-btn--raised mr-2 mb-2 mb-sm-0\">Contact Sales</a><a href=\"//www.pollfish.com/signup/researcher\" class=\"pf-btn pf-btn--primary pf-btn--xl\">Create an account</a></div></div></div></section><div aria-hidden='true' aria-labelledby='ContactSalesModal' class='modal contact-sales-modal' id='contact-sales-modal' role='dialog' tabindex='-1'><div class='modal-dialog modal-full-width' role='document'><div class='modal-content'><div class='modal-header flex-column'><div class='d-flex justify-content-between align-items-center w-100'><div class='header__logo'><a href=\"/\" class=\"logo\"><span class='icon-logo header__logo-icon'></span></a></div><div id='close-modal'></div></div></div><div class='modal-body d-flex flex-column'><div class='container'><div class='contact-form' id='contact-form'> Contact form </div></div></div></div></div></div><div aria-hidden='true' aria-labelledby='pollfishVideoModal' class='modal' id='videoModal' role='dialog' tabindex='-1'><div class='modal-dialog modal-lg' role='document'><div class='modal-content'><div class='modal-body pf-padding--0'><div class='embed-responsive embed-responsive-16by9'><iframe class='embed-responsive-item' src='//www.youtube.com/embed/fj8657hj7hc?rel=0'></iframe></div></div></div></div></div></main><div class='trusted-by text-center'><div class='container trusted-by__container'><h3 class='trusted-by__title'>Powering research for some of the world's top brands</h3><div class='row'><div class='col-md-4 col-lg-3 trusted-by__cell'><span class='icon-microsoft pf-font-size--24 trusted-by__icon trusted-by__icon--white'></span></div><div class='col-md-4 col-lg-2 trusted-by__cell'><span class='icon-tmobile pf-font-size--28 trusted-by__icon trusted-by__icon--white'></span></div><div class='col-md-4 col-lg-2 trusted-by__cell'><span class='icon-omd pf-font-size--28 trusted-by__icon trusted-by__icon--white'></span></div><div class='col-md-4 col-lg-2 trusted-by__cell'><span class='icon-millward pf-font-size--24 trusted-by__icon trusted-by__icon--white'></span></div><div class='col-md-4 col-lg-3 trusted-by__cell'><span class='icon-oracle pf-font-size--16 trusted-by__icon trusted-by__icon--white'></span></div></div></div></div><footer class='footer'><div class='footer-menu'><div class='container'><div class='row'><div class='col-12 col-sm-4 pf-col-xs-6 col-lg-2'><h3 class='footer-menu__title'>Company</h3><ul class='footer-menu__list'><li class='footer-menu__item'><a href='//www.pollfish.com/contact'>Contact</a></li><li class='footer-menu__item'><a href='//help.pollfish.com' target='_blank'>Pollfish Help Center</a></li><li class='footer-menu__item'><a href='//www.pollfish.com/company/about'>About</a></li><li class='footer-menu__item'><a href='//www.pollfish.com/company/team'>Team</a></li><li class='footer-menu__item'><a href='//pollfish.workable.com'>Careers</a></li><li class='footer-menu__item'><a href='//www.pollfish.com/blog' target='_blank'>Blog</a></li><li class='footer-menu__item'><a href='//www.pollfish.com/company/mediakit'>Media Kit</a></li></ul></div><div class='col-12 col-sm-4 pf-col-xs-6 col-lg-2'><h3 class='footer-menu__title'>How it works</h3><ul class='footer-menu__list'><li class='footer-menu__item'><a href=\"/audience\">Audience</a></li><li class='footer-menu__item'><a href=\"/questionnaire\">Questionnaire</a></li><li class='footer-menu__item'><a href=\"/results\">Results</a></li></ul></div><div class='col-12 col-sm-4 pf-col-xs-6 col-lg-2'><h3 class='footer-menu__title'>Features</h3><ul class='footer-menu__list'><li class='footer-menu__item'><a href='//www.pollfish.com/lp/nps'>NPS Surveys</a></li><li class='footer-menu__item'><a href='//www.pollfish.com/lp/pollfish-personas'>Pollfish Personas</a></li><li class='footer-menu__item'><a href='//www.pollfish.com/lp/logo-poll'>Concept testing</a></li><li class='footer-menu__item'><a href='//www.pollfish.com/lp/behind-enemy-lines'>Target per Installed app</a></li><li class='footer-menu__item'><a href='//www.pollfish.com/lp/google-consumer-surveys-alternative'>Pollfish vs Google</a></li><li class='footer-menu__item'><a href='//www.pollfish.com/lp/survey-monkey-audience-alternative'>Pollfish vs Survey Monkey Audience</a></li><li class='footer-menu__item'><a href='//www.pollfish.com/lp/survata-alternative'>Pollfish vs Survata</a></li></ul></div><div class='col-12 col-sm-4 pf-col-xs-6 col-lg-2'><h3 class='footer-menu__title'>Researchers</h3><ul class='footer-menu__list pf-margin--bottom-8'><li class='footer-menu__item'><a href='/pricing'>Pricing</a></li><li class='footer-menu__item'><a href='//pollfish.zendesk.com/hc/en-us/sections/201293921-Researchers' target='_blank'>FAQ</a></li><li class='footer-menu__item'><a href='//www.pollfish.com/blog/category/market-research/' target='_blank'>Case studies</a></li><li class='footer-menu__item'><a href='//www.pollfish.com/pf/buy-survey-responses/' target='_blank'>Buy survey responses</a></li></ul><h3 class='footer-menu__title'>Respondents</h3><ul class='footer-menu__list'><li class='footer-menu__item'><a href='//www.pollfish.com/respondent/opt-out'>Opt out</a></li></ul></div><div class='col-12 col-sm-4 pf-col-xs-6 col-lg-2'><h3 class='footer-menu__title'>Publishers</h3><ul class='footer-menu__list pf-margin--bottom-8'><li class='footer-menu__item'><a href='//www.pollfish.com/docs/android/google-play'>Android documentation</a></li><li class='footer-menu__item'><a href='//www.pollfish.com/docs/ios'>iOS documentation</a></li><li class='footer-menu__item'><a href='//www.pollfish.com/docs/webplugin'>Web Plugin documentation</a></li><li class='footer-menu__item'><a href='//www.pollfish.com/docs/unity'>Other Plugins</a></li><li class='footer-menu__item'><a href='//www.pollfish.com/docs/s2s'>Server-to-Server Callbacks</a></li><li class='footer-menu__item'><a href='//www.pollfish.com/docs/rewarded-surveys'>Rewarded Surveys</a></li><li class='footer-menu__item'><a href='//www.pollfish.com/publisher/referral'>Referral</a></li><li class='footer-menu__item'><a href='//www.pollfish.com/publisher/app-case-studies'>App Case Studies</a></li><li class='footer-menu__item'><a href='//www.pollfish.com/publisher/demo'>Demo Apps</a></li><li class='footer-menu__item'><a href='//pollfish.zendesk.com/hc/en-us/sections/201328652-Publishers'>FAQ</a></li></ul></div><div class='col-12 col-sm-4 pf-col-xs-6 col-lg-2'><h3 class='footer-menu__title'>Follow</h3><ul class='footer-menu__list pf-margin--bottom-8'><li class='footer-menu__item'><a href='https://www.facebook.com/pollfish' target='_blank'>Facebook</a></li><li class='footer-menu__item'><a href='https://www.twitter.com/pollfish' target='_blank'>Twitter</a></li><li class='footer-menu__item'><a href='https://www.linkedin.com/company/pollfish' target='_blank'>Linkedin</a></li></ul></div></div></div></div><hr><div class='container'><div class='row text-center'><div class='col-12'><div class='d-md-flex justify-content-center justify-content-between align-items-center'><ul class='footer-social-list list-inline'><span class='hidden-xs-down'>Terms of service:</span><li class='list-inline-item'><a href='https://www.pollfish.com/terms/researcher' target='_blank'>Researcher</a></li><li class='list-inline-item'><a href='https://www.pollfish.com/terms/publisher' target='_blank'>Publisher</a></li><li class='list-inline-item'><a href='https://www.pollfish.com/terms/respondent' target='_blank'>Respondent</a></li></ul><div> © 2018 Pollfish. All Rights Reserved </div></div></div></div></div></footer><script src=\"site.js\"></script><script>!function(){var analytics=window.analytics=window.analytics||[];if(!analytics.initialize)if(analytics.invoked)window.console&&console.error&&console.error(\"Segment snippet included twice.\");else{analytics.invoked=!0;analytics.methods=[\"trackSubmit\",\"trackClick\",\"trackLink\",\"trackForm\",\"pageview\",\"identify\",\"reset\",\"group\",\"track\",\"ready\",\"alias\",\"page\",\"once\",\"off\",\"on\"];analytics.factory=function(t){return function(){var e=Array.prototype.slice.call(arguments);e.unshift(t);analytics.push(e);return analytics}};for(var t=0;t<analytics.methods.length;t++){var e=analytics.methods[t];analytics[e]=analytics.factory(e)}analytics.load=function(t){var e=document.createElement(\"script\");e.type=\"text/javascript\";e.async=!0;e.src=(\"https:\"===document.location.protocol?\"https://\":\"http://\")+\"cdn.segment.com/analytics.js/v1/\"+t+\"/analytics.min.js\";var n=document.getElementsByTagName(\"script\")[0];n.parentNode.insertBefore(e,n)};analytics.SNIPPET_VERSION=\"3.1.0\";analytics.load(\"aevrmv2nk1\");analytics.page()}}();</script></body></html>");
    }

    public void load(@StringRes int dataRes) {
      load(Res.getString(dataRes));
    }

    public void load(String data) {
      load(data, "text/html", "UTF-8");
    }

    public void load(String data, String mimeType, String encoding) {
      this.mimeType = mimeType;
      this.encoding = encoding;
      show(data);
    }

    protected void show(String data) {
      this.data = data;
      this.key = System.identityHashCode(this);

      if (!listeners.isEmpty()) new BroadCastManager(context, key, listeners);

      Intent intent = new Intent(context, PollfishWebViewActivity.class);
      intent.putExtra("builder", this);

      Ctx.startActivity(intent);

      if (context instanceof Activity) {
        ((Activity) context).overridePendingTransition(animationOpenEnter, animationOpenExit);
      }
    }
  }
}
