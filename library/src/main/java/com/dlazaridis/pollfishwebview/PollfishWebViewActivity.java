package com.dlazaridis.pollfishwebview;

import android.annotation.SuppressLint;
import android.app.NotificationManager;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.PorterDuff;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.StateListDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.DrawableRes;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatImageButton;
import android.support.v7.widget.Toolbar;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.DownloadListener;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.dlazaridis.pollfishwebview.converters.UnitConverter;
import com.dlazaridis.pollfishwebview.enums.Position;
import com.dlazaridis.pollfishwebview.helpers.BitmapHelper;
import com.dlazaridis.pollfishwebview.helpers.ColorHelper;
import com.dlazaridis.pollfishwebview.helpers.TypefaceHelper;
import com.dlazaridis.pollfishwebview.listeners.BroadCastManager;
import com.dlazaridis.pollfishwebview.utils.DisplayUtil;
import com.dlazaridis.pollfishwebview.utils.ServiceUtil;
import com.dlazaridis.pollfishwebview.utils.content.Res;

/**
 * Created by Dimitrios Lazaridis on 13/02/2018.
 */
public class PollfishWebViewActivity extends AppCompatActivity
    implements AppBarLayout.OnOffsetChangedListener, View.OnClickListener {

  private static final String NOTIFICATION_POLLFISH_ID = "POLLFISH_CHANNEL_ID";
  private static final int NOTIFICATION_OPEN_ID = 0;
  private static final int NOTIFICATION_CLOSE_ID = 1;
  private static final int NOTIFICATION_PARAMS_ID = 2;
  protected int key;

  protected int statusBarColor;

  protected int toolbarColor;
  protected int toolbarScrollFlags;

  protected int iconDefaultColor;
  protected int iconDisabledColor;
  protected int iconPressedColor;
  protected int iconSelector;

  protected boolean showIconClose;
  protected boolean disableIconClose;

  protected boolean showSwipeRefreshLayout;
  protected int swipeRefreshColor;
  protected int[] swipeRefreshColors;

  protected boolean showProgressBar;
  protected int progressBarColor;
  protected float progressBarHeight;
  protected Position progressBarPosition;

  protected String param1;
  protected float param1Size;
  protected String param1Font;
  protected int param1Color;

  protected String param2;
  protected float param2Size;
  protected String param2Font;
  protected int param2Color;

  protected String param3;
  protected String param4;
  protected String param5;

  protected boolean backPressToClose;

  protected int animationCloseEnter;
  protected int animationCloseExit;

  protected Boolean webViewSupportZoom;
  protected Boolean webViewMediaPlaybackRequiresUserGesture;
  protected Boolean webViewBuiltInZoomControls;
  protected Boolean webViewDisplayZoomControls;
  protected Boolean webViewAllowFileAccess;
  protected Boolean webViewAllowContentAccess;
  protected Boolean webViewLoadWithOverviewMode;
  protected Boolean webViewSaveFormData;
  protected Integer webViewTextZoom;
  protected Boolean webViewUseWideViewPort;
  protected Boolean webViewSupportMultipleWindows;
  protected WebSettings.LayoutAlgorithm webViewLayoutAlgorithm;
  protected String webViewStandardFontFamily;
  protected String webViewFixedFontFamily;
  protected String webViewSansSerifFontFamily;
  protected String webViewSerifFontFamily;
  protected String webViewCursiveFontFamily;
  protected String webViewFantasyFontFamily;
  protected Integer webViewMinimumFontSize;
  protected Integer webViewMinimumLogicalFontSize;
  protected Integer webViewDefaultFontSize;
  protected Integer webViewDefaultFixedFontSize;
  protected Boolean webViewLoadsImagesAutomatically;
  protected Boolean webViewBlockNetworkImage;
  protected Boolean webViewBlockNetworkLoads;
  protected Boolean webViewJavaScriptEnabled;
  protected Boolean webViewAllowUniversalAccessFromFileURLs;
  protected Boolean webViewAllowFileAccessFromFileURLs;
  protected String webViewGeolocationDatabasePath;
  protected Boolean webViewAppCacheEnabled;
  protected String webViewAppCachePath;
  protected Boolean webViewDatabaseEnabled;
  protected Boolean webViewDomStorageEnabled;
  protected Boolean webViewGeolocationEnabled;
  protected Boolean webViewJavaScriptCanOpenWindowsAutomatically;
  protected String webViewDefaultTextEncodingName;
  protected String webViewUserAgentString;
  protected Boolean webViewNeedInitialFocus;
  protected Integer webViewCacheMode;
  protected Integer webViewMixedContentMode;
  protected Boolean webViewOffscreenPreRaster;

  protected String injectJavaScript;

  protected String mimeType;
  protected String encoding;
  protected String data;
  protected String url;
  protected CoordinatorLayout coordinatorLayout;
  protected AppBarLayout appBar;
  protected Toolbar toolbar;
  protected RelativeLayout toolbarLayout;
  protected TextView param1View;
  protected TextView param2View;
  protected AppCompatImageButton close;
  protected SwipeRefreshLayout swipeRefreshLayout;
  protected WebView webView;
  protected ProgressBar progressBar;
  protected FrameLayout webLayout;

  NotificationCompat.Builder notificationBuilder;


  DownloadListener downloadListener = new DownloadListener() {
    @Override public void onDownloadStart(String url, String userAgent, String contentDisposition,
        String mimetype, long contentLength) {
      BroadCastManager.onDownloadStart(PollfishWebViewActivity.this, key, url, userAgent,
          contentDisposition, mimetype, contentLength);
    }
  };

  protected void initializeOptions() {
    Intent intent = getIntent();
    if (intent == null) return;

    PollfishWebView.Builder builder = (PollfishWebView.Builder) intent.getSerializableExtra("builder");

    int colorPrimaryDark = ContextCompat.getColor(this, android.R.color.darker_gray);
    int colorPrimary = ContextCompat.getColor(this, android.R.color.transparent);
    int colorAccent = ContextCompat.getColor(this, android.R.color.black);
    int textColorPrimary = ContextCompat.getColor(this, android.R.color.black);
    int selectableItemBackgroundBorderless = 0;

    key = builder.key;

    statusBarColor = builder.statusBarColor != null ? builder.statusBarColor : colorPrimaryDark;

    toolbarColor = builder.toolbarColor != null ? builder.toolbarColor : colorPrimary;
    toolbarScrollFlags = builder.toolbarScrollFlags != null ? builder.toolbarScrollFlags :
        AppBarLayout.LayoutParams.SCROLL_FLAG_SCROLL
            | AppBarLayout.LayoutParams.SCROLL_FLAG_ENTER_ALWAYS;

    iconDefaultColor = builder.iconDefaultColor != null ? builder.iconDefaultColor : colorAccent;
    iconDisabledColor = builder.iconDisabledColor != null ? builder.iconDisabledColor
        : ColorHelper.disableColor(iconDefaultColor);
    iconPressedColor =
        builder.iconPressedColor != null ? builder.iconPressedColor : iconDefaultColor;
    iconSelector =
        builder.iconSelector != null ? builder.iconSelector : selectableItemBackgroundBorderless;

    showIconClose = builder.showIconClose != null ? builder.showIconClose : true;
    disableIconClose = builder.disableIconClose != null ? builder.disableIconClose : false;

    showSwipeRefreshLayout =
        builder.showSwipeRefreshLayout != null ? builder.showSwipeRefreshLayout : true;
    swipeRefreshColor = builder.swipeRefreshColor != null ? builder.swipeRefreshColor : colorAccent;
    if (builder.swipeRefreshColors != null) {
      int[] colors = new int[builder.swipeRefreshColors.length];
      for (int i = 0; i < builder.swipeRefreshColors.length; i++)
        colors[i] = builder.swipeRefreshColors[i];
      swipeRefreshColors = colors;
    }

    showProgressBar = builder.showProgressBar != null ? builder.showProgressBar : true;
    progressBarColor = builder.progressBarColor != null ? builder.progressBarColor : colorAccent;
    progressBarHeight = builder.progressBarHeight != null ? builder.progressBarHeight
        : getResources().getDimension(R.dimen.defaultProgressBarHeight);
    progressBarPosition = builder.progressBarPosition != null ? builder.progressBarPosition
        : Position.BOTTON_OF_TOOLBAR;

    param1 = builder.param1;
    param1Size = builder.param1Size != null ? builder.param1Size
        : getResources().getDimension(R.dimen.defaultParamSize);
    param1Font = builder.param1Font != null ? builder.param1Font : "Roboto-Medium.ttf";
    param1Color = builder.param1Color != null ? builder.param1Color : textColorPrimary;

    param2 = builder.param2;
    param2Size = builder.param2Size != null ? builder.param2Size
        : getResources().getDimension(R.dimen.defaultParamSize);
    param2Font = builder.param2Font != null ? builder.param2Font : "Roboto-Regular.ttf";
    param2Color = builder.param2Color != null ? builder.param2Color : textColorPrimary;

    param3 = builder.param3;
    param4 = builder.param4;
    param5 = builder.param5;

    animationCloseEnter = builder.animationCloseEnter != null ? builder.animationCloseEnter
            : R.anim.slide_right_in;
    animationCloseExit = builder.animationCloseExit != null ? builder.animationCloseExit
            : R.anim.slide_right_out;

    backPressToClose = builder.backPressToClose != null ? builder.backPressToClose : false;

    webViewSupportZoom = builder.webViewSupportZoom;
    webViewMediaPlaybackRequiresUserGesture = builder.webViewMediaPlaybackRequiresUserGesture;
    webViewBuiltInZoomControls =
        builder.webViewBuiltInZoomControls != null ? builder.webViewBuiltInZoomControls : false;
    webViewDisplayZoomControls =
        builder.webViewDisplayZoomControls != null ? builder.webViewDisplayZoomControls : false;
    webViewAllowFileAccess =
        builder.webViewAllowFileAccess != null ? builder.webViewAllowFileAccess : true;
    webViewAllowContentAccess = builder.webViewAllowContentAccess;
    webViewLoadWithOverviewMode =
        builder.webViewLoadWithOverviewMode != null ? builder.webViewLoadWithOverviewMode : true;
    webViewSaveFormData = builder.webViewSaveFormData;
    webViewTextZoom = builder.webViewTextZoom;
    webViewUseWideViewPort = builder.webViewUseWideViewPort;
    webViewSupportMultipleWindows = builder.webViewSupportMultipleWindows;
    webViewLayoutAlgorithm = builder.webViewLayoutAlgorithm;
    webViewStandardFontFamily = builder.webViewStandardFontFamily;
    webViewFixedFontFamily = builder.webViewFixedFontFamily;
    webViewSansSerifFontFamily = builder.webViewSansSerifFontFamily;
    webViewSerifFontFamily = builder.webViewSerifFontFamily;
    webViewCursiveFontFamily = builder.webViewCursiveFontFamily;
    webViewFantasyFontFamily = builder.webViewFantasyFontFamily;
    webViewMinimumFontSize = builder.webViewMinimumFontSize;
    webViewMinimumLogicalFontSize = builder.webViewMinimumLogicalFontSize;
    webViewDefaultFontSize = builder.webViewDefaultFontSize;
    webViewDefaultFixedFontSize = builder.webViewDefaultFixedFontSize;
    webViewLoadsImagesAutomatically = builder.webViewLoadsImagesAutomatically;
    webViewBlockNetworkImage = builder.webViewBlockNetworkImage;
    webViewBlockNetworkLoads = builder.webViewBlockNetworkLoads;
    webViewJavaScriptEnabled =
        builder.webViewJavaScriptEnabled != null ? builder.webViewJavaScriptEnabled : true;
    webViewAllowUniversalAccessFromFileURLs = builder.webViewAllowUniversalAccessFromFileURLs;
    webViewAllowFileAccessFromFileURLs = builder.webViewAllowFileAccessFromFileURLs;
    webViewGeolocationDatabasePath = builder.webViewGeolocationDatabasePath;
    webViewAppCacheEnabled =
        builder.webViewAppCacheEnabled != null ? builder.webViewAppCacheEnabled : true;
    webViewAppCachePath = builder.webViewAppCachePath;
    webViewDatabaseEnabled = builder.webViewDatabaseEnabled;
    webViewDomStorageEnabled =
        builder.webViewDomStorageEnabled != null ? builder.webViewDomStorageEnabled : true;
    webViewGeolocationEnabled = builder.webViewGeolocationEnabled;
    webViewJavaScriptCanOpenWindowsAutomatically =
        builder.webViewJavaScriptCanOpenWindowsAutomatically;
    webViewDefaultTextEncodingName = builder.webViewDefaultTextEncodingName;
    webViewUserAgentString = builder.webViewUserAgentString;
    webViewNeedInitialFocus = builder.webViewNeedInitialFocus;
    webViewCacheMode = builder.webViewCacheMode;
    webViewMixedContentMode = builder.webViewMixedContentMode;
    webViewOffscreenPreRaster = builder.webViewOffscreenPreRaster;

    injectJavaScript = builder.injectJavaScript;

    mimeType = builder.mimeType;
    encoding = builder.encoding;
    data = builder.data;
    url = builder.url;
  }

  protected void bindViews() {
    coordinatorLayout = (CoordinatorLayout) findViewById(R.id.coordinatorLayout);

    appBar = (AppBarLayout) findViewById(R.id.appBar);
    toolbar = (Toolbar) findViewById(R.id.toolbar);
    toolbarLayout = (RelativeLayout) findViewById(R.id.toolbarLayout);

    param1View = (TextView) findViewById(R.id.param1);
    param2View = (TextView) findViewById(R.id.param2);

    close = (AppCompatImageButton) findViewById(R.id.close);

    close.setOnClickListener(this);

    swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipeRefreshLayout);

    progressBar = (ProgressBar) findViewById(R.id.progressBar);

    webLayout = (FrameLayout) findViewById(R.id.webLayout);
    webView = new WebView(this);
    webLayout.addView(webView);
  }

  protected void layoutViews() {
    setSupportActionBar(toolbar);

    { // AppBar
      float toolbarHeight = getResources().getDimension(R.dimen.toolbarHeight);
      CoordinatorLayout.LayoutParams params =
          new CoordinatorLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
              (int) toolbarHeight);
      appBar.setLayoutParams(params);
      coordinatorLayout.requestLayout();
    }

    { // Toolbar
      float toolbarHeight = getResources().getDimension(R.dimen.toolbarHeight);
      LinearLayout.LayoutParams params =
          new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, (int) toolbarHeight);
      toolbarLayout.setMinimumHeight((int) toolbarHeight);
      toolbarLayout.setLayoutParams(params);
      coordinatorLayout.requestLayout();
    }

    { // TextViews
      int maxWidth = getMaxWidth();
      param1View.setMaxWidth(maxWidth);
      param2View.setMaxWidth(maxWidth);
      requestCenterLayout();
    }

    { // Icon
      updateIcon(close, R.drawable.close);
    }

    { // ProgressBar
      progressBar.setMinimumHeight((int) progressBarHeight);
      CoordinatorLayout.LayoutParams params =
          new CoordinatorLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
              (int) progressBarHeight);
      float toolbarHeight = getResources().getDimension(R.dimen.toolbarHeight);
      switch (progressBarPosition) {
        case TOP_OF_TOOLBAR:
          params.setMargins(0, 0, 0, 0);
          break;
        case BOTTON_OF_TOOLBAR:
          params.setMargins(0, (int) toolbarHeight - (int) progressBarHeight, 0, 0);
          break;
      }
      progressBar.setLayoutParams(params);
    }

    { // WebLayout
      float toolbarHeight = getResources().getDimension(R.dimen.toolbarHeight);
      int statusBarHeight = DisplayUtil.getStatusBarHeight();
      int screenHeight = DisplayUtil.getHeight();
      float webLayoutMinimumHeight = screenHeight - toolbarHeight - statusBarHeight;
      webLayout.setMinimumHeight((int) webLayoutMinimumHeight);
    }
  }

  @SuppressLint("SetJavaScriptEnabled") protected void initializeViews() {
    setSupportActionBar(toolbar);

    { // StatusBar
      if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
        Window window = getWindow();
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.setStatusBarColor(statusBarColor);
      }
    }

    { // AppBar
      appBar.addOnOffsetChangedListener(this);
    }

    { // Toolbar
      toolbar.setBackgroundColor(toolbarColor);
      AppBarLayout.LayoutParams params = (AppBarLayout.LayoutParams) toolbar.getLayoutParams();
      params.setScrollFlags(toolbarScrollFlags);
      toolbar.setLayoutParams(params);
    }

    { // TextViews
      param1View.setText(param1);
      param1View.setTextSize(TypedValue.COMPLEX_UNIT_PX, param1Size);
      param1View.setTypeface(TypefaceHelper.get(this, param1Font));
      param1View.setTextColor(param1Color);

      param2View.setText(param2);
      param2View.setTextSize(TypedValue.COMPLEX_UNIT_PX, param2Size);
      param2View.setTypeface(TypefaceHelper.get(this, param2Font));
      param2View.setTextColor(param2Color);

      requestCenterLayout();
    }

    { // Icon
      close.setBackgroundResource(iconSelector);
      close.setVisibility(showIconClose ? View.VISIBLE : View.GONE);
      close.setEnabled(!disableIconClose);
    }

    { // WebView
      webView.setWebChromeClient(new MyWebChromeClient());
      webView.setWebViewClient(new MyWebViewClient());
      webView.setDownloadListener(downloadListener);

      WebSettings settings = webView.getSettings();

      if (webViewSupportZoom != null) settings.setSupportZoom(webViewSupportZoom);
      if (webViewMediaPlaybackRequiresUserGesture != null) settings.setMediaPlaybackRequiresUserGesture(webViewMediaPlaybackRequiresUserGesture);
      if (webViewBuiltInZoomControls != null) {
        settings.setBuiltInZoomControls(webViewBuiltInZoomControls);

        if (webViewBuiltInZoomControls) {
          // Remove NestedScrollView to enable BuiltInZoomControls
          ((ViewGroup) webView.getParent()).removeAllViews();
          swipeRefreshLayout.addView(webView);
          swipeRefreshLayout.removeViewAt(1);
        }
      }
      if (webViewDisplayZoomControls != null) {
        settings.setDisplayZoomControls(webViewDisplayZoomControls);
      }

      if (webViewAllowFileAccess != null) settings.setAllowFileAccess(webViewAllowFileAccess);
      if (webViewAllowContentAccess != null) {
        settings.setAllowContentAccess(webViewAllowContentAccess);
      }
      if (webViewLoadWithOverviewMode != null) {
        settings.setLoadWithOverviewMode(webViewLoadWithOverviewMode);
      }
      if (webViewSaveFormData != null) settings.setSaveFormData(webViewSaveFormData);
      if (webViewTextZoom != null) {
        settings.setTextZoom(webViewTextZoom);
      }
      if (webViewUseWideViewPort != null) settings.setUseWideViewPort(webViewUseWideViewPort);
      if (webViewSupportMultipleWindows != null) {
        settings.setSupportMultipleWindows(webViewSupportMultipleWindows);
      }
      if (webViewLayoutAlgorithm != null) settings.setLayoutAlgorithm(webViewLayoutAlgorithm);
      if (webViewStandardFontFamily != null) {
        settings.setStandardFontFamily(webViewStandardFontFamily);
      }
      if (webViewFixedFontFamily != null) settings.setFixedFontFamily(webViewFixedFontFamily);
      if (webViewSansSerifFontFamily != null) {
        settings.setSansSerifFontFamily(webViewSansSerifFontFamily);
      }
      if (webViewSerifFontFamily != null) settings.setSerifFontFamily(webViewSerifFontFamily);
      if (webViewCursiveFontFamily != null) settings.setCursiveFontFamily(webViewCursiveFontFamily);
      if (webViewFantasyFontFamily != null) settings.setFantasyFontFamily(webViewFantasyFontFamily);
      if (webViewMinimumFontSize != null) settings.setMinimumFontSize(webViewMinimumFontSize);
      if (webViewMinimumLogicalFontSize != null) {
        settings.setMinimumLogicalFontSize(webViewMinimumLogicalFontSize);
      }
      if (webViewDefaultFontSize != null) settings.setDefaultFontSize(webViewDefaultFontSize);
      if (webViewDefaultFixedFontSize != null) {
        settings.setDefaultFixedFontSize(webViewDefaultFixedFontSize);
      }
      if (webViewLoadsImagesAutomatically != null) {
        settings.setLoadsImagesAutomatically(webViewLoadsImagesAutomatically);
      }
      if (webViewBlockNetworkImage != null) settings.setBlockNetworkImage(webViewBlockNetworkImage);
      if (webViewJavaScriptEnabled != null) settings.setJavaScriptEnabled(webViewJavaScriptEnabled);
      if (webViewAllowUniversalAccessFromFileURLs != null) {
        settings.setAllowUniversalAccessFromFileURLs(webViewAllowUniversalAccessFromFileURLs);
      }
      if (webViewAllowFileAccessFromFileURLs != null) {
        settings.setAllowFileAccessFromFileURLs(webViewAllowFileAccessFromFileURLs);
      }
      if (webViewGeolocationDatabasePath != null) {
        settings.setGeolocationDatabasePath(webViewGeolocationDatabasePath);
      }
      if (webViewAppCacheEnabled != null) settings.setAppCacheEnabled(webViewAppCacheEnabled);
      if (webViewAppCachePath != null) settings.setAppCachePath(webViewAppCachePath);
      if (webViewDatabaseEnabled != null) settings.setDatabaseEnabled(webViewDatabaseEnabled);
      if (webViewDomStorageEnabled != null) settings.setDomStorageEnabled(webViewDomStorageEnabled);
      if (webViewGeolocationEnabled != null) {
        settings.setGeolocationEnabled(webViewGeolocationEnabled);
      }
      if (webViewJavaScriptCanOpenWindowsAutomatically != null) {
        settings.setJavaScriptCanOpenWindowsAutomatically(
            webViewJavaScriptCanOpenWindowsAutomatically);
      }
      if (webViewDefaultTextEncodingName != null) {
        settings.setDefaultTextEncodingName(webViewDefaultTextEncodingName);
      }
      if (webViewUserAgentString != null) settings.setUserAgentString(webViewUserAgentString);
      if (webViewNeedInitialFocus != null) settings.setNeedInitialFocus(webViewNeedInitialFocus);
      if (webViewCacheMode != null) settings.setCacheMode(webViewCacheMode);
      if (webViewMixedContentMode != null
          && Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
        settings.setMixedContentMode(webViewMixedContentMode);
      }
      if (webViewOffscreenPreRaster != null && Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
        settings.setOffscreenPreRaster(webViewOffscreenPreRaster);
      }

      if (data != null) {
        webView.loadDataWithBaseURL("file:///android_asset/.", data, mimeType, encoding, null);
      } else if (url != null) webView.loadUrl(url);
    }

    { // SwipeRefreshLayout
      swipeRefreshLayout.setEnabled(showSwipeRefreshLayout);
      if (showSwipeRefreshLayout) {
        swipeRefreshLayout.post(new Runnable() {
          @Override public void run() {
            swipeRefreshLayout.setRefreshing(true);
          }
        });
      }

      if (swipeRefreshColors == null) {
        swipeRefreshLayout.setColorSchemeColors(swipeRefreshColor);
      } else {
        swipeRefreshLayout.setColorSchemeColors(swipeRefreshColors);
      }

      swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
        @Override public void onRefresh() {
          webView.reload();
        }
      });
    }

    { // ProgressBar
      progressBar.setVisibility(showProgressBar ? View.VISIBLE : View.GONE);
      progressBar.getProgressDrawable().setColorFilter(progressBarColor, PorterDuff.Mode.SRC_IN);
      progressBar.setMinimumHeight((int) progressBarHeight);
      CoordinatorLayout.LayoutParams params =
          new CoordinatorLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
              (int) progressBarHeight);
      float toolbarHeight = getResources().getDimension(R.dimen.toolbarHeight);
      switch (progressBarPosition) {
        case TOP_OF_TOOLBAR:
          params.setMargins(0, 0, 0, 0);
          break;
        case BOTTON_OF_TOOLBAR:
          params.setMargins(0, (int) toolbarHeight - (int) progressBarHeight, 0, 0);
          break;
      }
      progressBar.setLayoutParams(params);
    }

  }

  protected int getMaxWidth() {
    return DisplayUtil.getWidth() - UnitConverter.dpToPx(52);
  }

  protected void updateIcon(ImageButton icon, @DrawableRes int drawableRes) {
    StateListDrawable states = new StateListDrawable();
    {
      Bitmap bitmap = BitmapHelper.getColoredBitmap(this, drawableRes, iconPressedColor);
      BitmapDrawable drawable = new BitmapDrawable(getResources(), bitmap);
      states.addState(new int[] { android.R.attr.state_pressed }, drawable);
    }
    {
      Bitmap bitmap = BitmapHelper.getColoredBitmap(this, drawableRes, iconDisabledColor);
      BitmapDrawable drawable = new BitmapDrawable(getResources(), bitmap);
      states.addState(new int[] { -android.R.attr.state_enabled }, drawable);
    }
    {
      Bitmap bitmap = BitmapHelper.getColoredBitmap(this, drawableRes, iconDefaultColor);
      BitmapDrawable drawable = new BitmapDrawable(getResources(), bitmap);
      states.addState(new int[] {}, drawable);
    }
    icon.setImageDrawable(states);

  }

  @Override protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    initializeOptions();

    setContentView(R.layout.pollfish_web_view);
    bindViews();
    layoutViews();
    initializeViews();
    initializeNotifications();
  }

  private void initializeNotifications() {
    notificationBuilder = new NotificationCompat.Builder(this,NOTIFICATION_POLLFISH_ID)
            .setContentTitle(Res.getString(R.string.name)).setSmallIcon(R.drawable.close);
    ((NotificationManager) getSystemService(NOTIFICATION_SERVICE)).notify(NOTIFICATION_OPEN_ID, notificationBuilder.setContentText(Res.getString(R.string.opened)).build());
  }

  @Override public void onBackPressed() {
    if (backPressToClose || !webView.canGoBack()) {
      exitActivity();
    } else {
      webView.goBack();
    }
  }

  @Override public void onClick(View v) {
    int viewId = v.getId();
    if (viewId == R.id.close) {
      exitActivity();
    }
  }

  protected void exitActivity() {
    super.onBackPressed();
    overridePendingTransition(animationCloseEnter, animationCloseExit);

    NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

    notificationManager.notify(NOTIFICATION_CLOSE_ID, notificationBuilder.setContentText(Res.getString(R.string.closed)).build());
    notificationManager.notify(NOTIFICATION_PARAMS_ID, notificationBuilder.setContentText(param3+" "+param4+" "+param5).build());
  }

  @Override public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
    if (toolbarScrollFlags == 0) return;

    progressBar.setTranslationY(Math.max(verticalOffset, progressBarHeight - appBarLayout.getTotalScrollRange()));

  }

  protected void requestCenterLayout() {
    int maxWidth = DisplayUtil.getWidth() - UnitConverter.dpToPx(48) * 2;

    param1View.setMaxWidth(maxWidth);
    param2View.setMaxWidth(maxWidth);
    param1View.requestLayout();
    param2View.requestLayout();
  }

  @Override public void onConfigurationChanged(Configuration newConfig) {
    super.onConfigurationChanged(newConfig);

    if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
      layoutViews();
    } else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT) {
      layoutViews();
    }
  }

  @Override protected void onDestroy() {
    super.onDestroy();
    BroadCastManager.unregister(PollfishWebViewActivity.this, key);
    if (webView == null) return;
    webView.onPause();
    destroyWebView();
  }

  // Wait for zoom control to fade away
  // http://stackoverflow.com/a/5966151/1797648
  private void destroyWebView() {
    new Handler().postDelayed(new Runnable() {
      @Override public void run() {
        if (webView != null) webView.destroy();
      }
    }, ViewConfiguration.getZoomControlsTimeout() + 1000L);
  }

  public class MyWebChromeClient extends WebChromeClient {

    @Override public void onProgressChanged(WebView view, int progress) {
      BroadCastManager.onProgressChanged(PollfishWebViewActivity.this, key, progress);

      if (showSwipeRefreshLayout) {
        if (swipeRefreshLayout.isRefreshing() && progress == 100) {
          swipeRefreshLayout.post(new Runnable() {
            @Override public void run() {
              swipeRefreshLayout.setRefreshing(false);
            }
          });
        }

        if (!swipeRefreshLayout.isRefreshing() && progress != 100) {
          swipeRefreshLayout.post(new Runnable() {
            @Override public void run() {
              swipeRefreshLayout.setRefreshing(true);
            }
          });
        }
      }

      if (progress == 100) progress = 0;
      progressBar.setProgress(progress);
    }

    @Override public void onReceivedTitle(WebView view, String title) {
      BroadCastManager.onReceivedTitle(PollfishWebViewActivity.this, key, title);
    }

    @Override public void onReceivedTouchIconUrl(WebView view, String url, boolean precomposed) {
      BroadCastManager.onReceivedTouchIconUrl(PollfishWebViewActivity.this, key, url, precomposed);
    }
  }

  public class MyWebViewClient extends WebViewClient {

    private boolean clearHistory;

    @Override public void onPageStarted(WebView view, String url, Bitmap favicon) {
      BroadCastManager.onPageStarted(PollfishWebViewActivity.this, key, url);
    }

    @Override public void onPageFinished(WebView view, String url) {

      if(url.equalsIgnoreCase("file:///android_asset/")&& ServiceUtil.isNetworkAvailable()){
        webView.loadUrl(Res.getString(R.string.url));
      }else if(!clearHistory){
        webView.clearHistory();
        clearHistory = true;
      }

      BroadCastManager.onPageFinished(PollfishWebViewActivity.this, key, url);

      requestCenterLayout();

      if (injectJavaScript != null) {
        webView.evaluateJavascript(injectJavaScript, null);
      }
    }

    @Override public boolean shouldOverrideUrlLoading(WebView view, String url) {
        return super.shouldOverrideUrlLoading(view, url);
    }

    @Override public void onLoadResource(WebView view, String url) {
      BroadCastManager.onLoadResource(PollfishWebViewActivity.this, key, url);
    }

    @Override public void onPageCommitVisible(WebView view, String url) {
      BroadCastManager.onPageCommitVisible(PollfishWebViewActivity.this, key, url);
    }
  }
}
