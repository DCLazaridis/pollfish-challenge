package com.dlazaridis.pollfishwebview.converters;

import com.dlazaridis.pollfishwebview.Base;

/**
 * Created by Dimitrios Lazaridis on 13/2/2018.
 */

public class UnitConverter {

    public static int dpToPx(int dp) {
        return (int) (dp * Base.getDisplayMetrics().density + 0.5f);
    }

}
