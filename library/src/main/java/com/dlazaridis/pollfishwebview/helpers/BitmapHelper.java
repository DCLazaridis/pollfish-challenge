package com.dlazaridis.pollfishwebview.helpers;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.support.annotation.ColorInt;
import android.support.annotation.DrawableRes;
import android.support.annotation.NonNull;

/**
 * Created by Dimitrios Lazaridis on 13/02/2018.
 */
public class BitmapHelper {

  private BitmapHelper() {
  }

  public static Bitmap getColoredBitmap(@NonNull Bitmap bitmap, @ColorInt int color) {
    int alpha = Color.alpha(color);
    int red = Color.red(color);
    int green = Color.green(color);
    int blue = Color.blue(color);

    int[] pixels = new int[bitmap.getWidth() * bitmap.getHeight()];
    bitmap.getPixels(pixels, 0, bitmap.getWidth(), 0, 0, bitmap.getWidth(), bitmap.getHeight());
    for (int i = 0; i < pixels.length; i++) {
      int pixel = pixels[i];
      int pixelAlpha = Color.alpha(pixel);
      if (pixelAlpha != 0) {
        pixels[i] = Color.argb((int) (pixelAlpha * alpha / 256f), red, green, blue);
      }
    }

    Bitmap coloredBitmap = bitmap.copy(Bitmap.Config.ARGB_8888, true);
    coloredBitmap.setPixels(pixels, 0, bitmap.getWidth(), 0, 0, bitmap.getWidth(),
        bitmap.getHeight());
    return coloredBitmap;
  }

  public static Bitmap getColoredBitmap(@NonNull Context context, @DrawableRes int drawableRes,
      @ColorInt int color) {
    Bitmap bitmap = BitmapFactory.decodeResource(context.getResources(), drawableRes);
    return getColoredBitmap(bitmap, color);
  }

}
