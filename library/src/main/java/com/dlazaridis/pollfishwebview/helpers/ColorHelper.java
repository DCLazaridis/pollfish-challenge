package com.dlazaridis.pollfishwebview.helpers;

import android.graphics.Color;

/**
 * Created by Dimitrios Lazaridis on 13/02/2018.
 */
public class ColorHelper {

  private ColorHelper() {
  }

  public static int disableColor(int color) {
    int alpha = Color.alpha(color);
    int red = Color.red(color);
    int green = Color.green(color);
    int blue = Color.blue(color);

    return Color.argb((int) (alpha * 0.2f), red, green, blue);
  }
}
