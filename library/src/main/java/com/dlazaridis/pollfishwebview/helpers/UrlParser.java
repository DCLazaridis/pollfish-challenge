package com.dlazaridis.pollfishwebview.helpers;

import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by Dimitrios Lazaridis on 13/02/2018.
 */
public class UrlParser {

  private UrlParser() {
  }

  public static String getHost(String url) {
    try {
      return new URL(url).getHost();
    } catch (MalformedURLException e) {
      e.printStackTrace();
    }
    return url;
  }
}
