package com.dlazaridis.pollfishwebview.utils;

import android.graphics.Point;
import android.view.Display;

/**
 * Created by Dimitrios Lazaridis on 13/2/2018.
 */

public class DisplayUtil {

    public static int getWidth() {
        Display display = WindowManagerUtil.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        return size.x;
    }

    public static int getHeight() {
        Display display = WindowManagerUtil.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        return size.y;
    }

    public static int getStatusBarHeight() {
        int resourceId = ResourcesUtil.getIdentifier("status_bar_height", "dimen", "android");
        return resourceId > 0 ?
                ResourcesUtil.getDimensionPixelSize(resourceId) :
                0;
    }

}
