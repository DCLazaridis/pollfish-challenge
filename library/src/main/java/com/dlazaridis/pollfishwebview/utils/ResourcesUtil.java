package com.dlazaridis.pollfishwebview.utils;

import android.support.annotation.DimenRes;

import com.dlazaridis.pollfishwebview.Base;

/**
 * Created by Dimitrios Lazaridis on 13/2/2018.
 */

public class ResourcesUtil {

    public static int getIdentifier(String name, String defType, String defPackage) {
        return Base.getResources().getIdentifier(name, defType, defPackage);
    }

    public static int getDimensionPixelSize(@DimenRes int dimenRes) {
        return Base.getResources().getDimensionPixelSize(dimenRes);
    }

}
