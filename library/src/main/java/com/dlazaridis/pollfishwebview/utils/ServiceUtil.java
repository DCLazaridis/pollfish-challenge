package com.dlazaridis.pollfishwebview.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.annotation.NonNull;
import android.view.WindowManager;

import com.dlazaridis.pollfishwebview.Base;

/**
 * Created by Dimitrios Lazaridis on 13/2/2018.
 */

public class ServiceUtil {

    public static Object getSystemService(@NonNull String serviceName) {
        return Base.getContext().getSystemService(serviceName);
    }

    public static WindowManager getWindowManager() {
        return (WindowManager) getSystemService(Context.WINDOW_SERVICE);
    }

    public static boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

}
