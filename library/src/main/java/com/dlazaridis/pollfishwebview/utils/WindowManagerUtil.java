package com.dlazaridis.pollfishwebview.utils;

import android.view.Display;

/**
 * Created by Dimitrios Lazaridis on 13/2/2018.
 */

public class WindowManagerUtil {

    public static Display getDefaultDisplay() {
        return ServiceUtil.getWindowManager().getDefaultDisplay();
    }

}
