package com.dlazaridis.pollfishwebview.utils.content;

import android.annotation.TargetApi;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.ColorInt;
import android.support.annotation.ColorRes;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;

import com.dlazaridis.pollfishwebview.Base;

/**
 * Created by Dimitrios Lazaridis on 13/2/2018.
 */

public class Ctx {

    public static void startActivity(@NonNull Intent intent) {
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        Base.getContext().startActivity(intent);
    }

    @TargetApi(16)
    public static void startActivity(Intent intent, Bundle options) {
        Base.getContext().startActivity(intent, options);
    }

    @ColorInt
    public static int getColor(@ColorRes int colorRes) {
        return ContextCompat.getColor(Base.getContext(), colorRes);
    }

}
