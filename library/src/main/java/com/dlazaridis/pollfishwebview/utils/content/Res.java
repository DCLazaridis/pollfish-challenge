package com.dlazaridis.pollfishwebview.utils.content;

import android.support.annotation.ArrayRes;
import android.support.annotation.ColorInt;
import android.support.annotation.ColorRes;
import android.support.annotation.DimenRes;
import android.support.annotation.StringRes;

import com.dlazaridis.pollfishwebview.Base;

/**
 * Created by Dimitrios Lazaridis on 13/2/2018.
 */

public class Res {

    @ColorInt
    public static int getColor(@ColorRes int colorRes) {
        return Ctx.getColor(colorRes);
    }

    public static int[] getIntArray(@ArrayRes int arrayRes) {
        return Base.getResources().getIntArray(arrayRes);
    }

    public static float getDimension(@DimenRes int dimenRes) {
        return Base.getResources().getDimension(dimenRes);
    }

    public static String getString(@StringRes int stringRes) {
        return Base.getResources().getString(stringRes);
    }

}
